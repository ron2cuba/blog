<?php
declare(strict_types=1);

use Core\EngineController;
use Symfony\Component\HttpFoundation\Request;
require_once '../vendor/autoload.php';
/**
 * 1 Création de la requete qui correspond
 * 2 Nous allons voir toutes les routes qui existes
 * 3 UrlMatcher analyse la requete et connait nom de la route et nom du callable
 * 4 Index.php appelle cette fonction avec call_user_func qui a dans ses param
 * les infos que je souhaite envoyer à mon callable, stocker des infos dans le 
 * request paramBag si necessaire.
 */
/**
 * Creates a new request with values from PHP's super globals.
 */
$request = Request::createFromGlobals();
$engine = new EngineController();

$response = $engine->handle($request);

$response->send();