<?php
declare(strict_types=1);

namespace Helpers;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class HelperMethods{
    /**
     * Transform string into slug
     *
     * @param string $string String to slugify
     * @return string
     */
    public static function slugify($string): string{
        // Match a single character not present in the list below [^A-Za-z0-9-]
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string), '-'));
    }

    /**
     * notify about the status of the interaction with the database
     *
     * @param string $text
     * @param string $background
     * @return void
     */
    public static function notify(string $text, string $background, string $textColor)
    {
        echo '<div style="background-color: '.$background.';color:'.$textColor.';
        text-align:center;"><strong>'.$text.'</strong></div>';
    }

    /**
     * check if image can be uploaded
     *
     * @param UploadedFile $image
     * @return boolean
     */
    public static function validateImageFormat(UploadedFile $image):bool
    {
        $accepted = [
            'image/jpg',
            'image/png',
            'image/jpeg',
            'image/gif'
            ];
        
        if(!in_array($image->getClientMimeType(), $accepted)){
            // ajouter erreur au tableau 
            HelperMethods::notify(
                'type de fichier non accepté sur le serveur', "#FF0000", "#fff"
            );
            return false;
        }
        return true;

    }

    /**
     * verify if image file is under 200 000 bytes
     *
     * @param UploadedFile $image
     * @return boolean
     */
    public static function validateSize(UploadedFile $image):bool
    {
        if($image->getSize() > 2000000){
            HelperMethods::notify('Fichier trop volumineux', "#FF0000", "#fff");
            return false;
        }
        return true;
    }

    /**
     * to validate email
     *
     * @param string $email
     * @return boolean
     */
    public static function validateEmail(string $email):bool
    {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            HelperMethods::notify('Vous devez saisir un email valide', "#FF0000", "#fff");
            return false;
        }
        return true;
    }
}