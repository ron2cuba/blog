<?php
declare(strict_types=1);

namespace App\Utils;

use App\Utils\Form;

$routes = include '../src/routes.php';

$form = new Form();

if($adminRights > 0){
    $form->startForm('post', $routes->get('createPost')->getPath(), [
        'class' => 'form',
        'id' => 'createPostId'
        ])
        // input title
        ->startFieldSet()
        ->addLabel("title", "Titre*", [])
        ->addSpan("span-error", $errors['title'] ?? '',[
            'class' => 'error',
            'style' => 'color:red;padding: .2rem;font-size: .5rem;'
        ])
        ->addInput("text", "title", [
            'class' => 'title',
            'value' => htmlentities($request->request->get('title') ?? '')
            ])
        ->endFieldSet()
        // Select
        ->startFieldSet()
        ->addLabel("select", "Catégorie*")
        ->addSelect("categories", $default = null, $categories, ['class' => 'categorie'])
        ->endFieldSet()
        // textarea
        ->startFieldSet()
        ->addLabel("texarea", "Votre texte*")
        ->addSpan("span-error", $errors['textarea'] ?? '',[
            'class' => 'error',
            'style' => 'color:red;padding: .2rem;font-size: .5rem;'
        ])
        ->addTextarea("textarea", htmlentities($request->request->get('textarea') ?? ''), ['id' => 'content',
        'cols' => 30,
        'rows' => 10])
        ->endFieldSet()
        // image
        ->addSpan("span-error", $errors['image'] ?? '',[
            'class' => 'error',
            'style' => 'color:red;padding: .2rem;font-size: .5rem;'
        ])
        ->startFieldSet()
        ->addLabel('image', 'Image d\'illustration* (format: jpg, jpeg, gif, png)', [])
        ->addInput("file", "post_image", [
            'id'=>'post_image',
            'class'=>'post_image'
        ])
        // checkbox
        ->addLabel("is_published", "Publier", [])
        ->addCheckBox("is_published", "is_published", ['style' => 'margin-bottom: 1rem;'])
        // submit
        ->addDiv("*champs obligatoires" ,['class'=>'required'])
        ->closeDiv()
        ->startFieldSet()
        ->addButton("Enregistrer", ['type' => 'submit', 'name' => 'submit','class' => 'submit'])
        ->endFieldSet()
        ->endForm();
} else {
    $form->startForm('post', $routes->get('createPost')->getPath(), [
        'class' => 'form',
        'id' => 'createPostId'
        ])
        // input title
        ->startFieldSet()
        ->addLabel("title", "Titre*", [])
        ->addSpan("span-error", $errors['title'] ?? '',[
            'class' => 'error',
            'style' => 'color:red;padding: .2rem;font-size: .5rem;'
        ])
        ->addInput("text", "title", [
            'class' => 'title',
            'value' => !empty($request->get('title')) ? htmlentities($request->get('title')) : ''
            ])
        ->endFieldSet()
        // Select
        ->startFieldSet()
        ->addLabel("select", "Catégorie*")
        ->addSelect("categories", $default = null, $categories, ['class' => 'categorie'])
        ->endFieldSet()
        // textarea
        ->startFieldSet()
        ->addLabel("texarea", "Votre texte*")
        ->addSpan("span-error", $errors['textarea'] ?? '',[
            'class' => 'error',
            'style' => 'color:red;padding: .2rem;font-size: .5rem;'
        ])
        ->addTextarea("textarea", htmlentities($request->get('textarea') ?? ''), ['id' => 'content',
        'cols' => 30,
        'rows' => 10])
        ->endFieldSet()
        // image
        ->addSpan("span-error", $errors['image'] ?? '',[
            'class' => 'error',
            'style' => 'color:red;padding: .2rem;font-size: .5rem;'
        ])
        ->startFieldSet()
        ->addLabel('image', 'Image d\'illustration* (format: jpg, jpeg, gif, png)', [])
        ->addInput("file", "post_image", [
            'id'=>'post_image',
            'class'=>'post_image'
        ])
        // submit
        ->addDiv("*champs obligatoires" ,['class'=>'required'])
        ->closeDiv()
        ->startFieldSet()
        ->addButton("Enregistrer", ['type' => 'submit', 'name' => 'submit','class' => 'submit'])
        ->endFieldSet()
        ->endForm();
}
