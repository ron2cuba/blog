<?php
declare(strict_types=1);

namespace App\Utils;

$form = new Form();

$form->startForm('post', $slug, [
    'class' => 'form',
    'id' => 'updateUser'
])

->startFieldSet()
// username
->addLabel('username', "modifier username", [])
->addInput("text", "username", [
    'value' => $accesToUser->username
])
->endFieldSet()
->startFieldSet()
// email
->addLabel("email", "modifier email", [])
->addInput("email", "email", [
    'value' => $accesToUser->email
])
->endFieldSet()
// image
->startFieldSet()
->addLabel('user_image', "modifier son avatar", [])
->addInput('file', 'user_image', [])
->endFieldSet()
// textarea
->startFieldSet()
->addLabel('textarea', "modifier biography", [])
->addTextarea("textarea", $accesToUser->content, [
    'rows' => 10,
    'cols' => 30
])
->endFieldSet()
// actual password
->addLabel('password', "mot de passe actuel", [])
->addInput("password", "confirm-actual")
// password
->addLabel('password', "modifier password", [])
->addInput("password", "password")
//repeat
->addLabel('password-repeat', "repeat password", [])
->addInput("password", "password-repeat")

// submit
->startFieldSet()
->addButton("Enregistrer", ['type' => 'submit', 'name' => 'submit','class' => 'submit'])
->endFieldSet()
->addDiv("Mes posts :", [
    'class'=>'post-list'
])
->endForm();