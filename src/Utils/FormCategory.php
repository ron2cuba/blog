<?php
declare(strict_types=1);

namespace App\Utils;
$routes = include '../src/routes.php';

$form = new Form();

$form->startForm('post', $routes->get('createCategory')->getPath(), [
    'class' => 'form',
    'id' => 'createCategoryId'
    ])
    // input title
    ->startFieldSet()
    ->addSpan("span-error", $errors['title'] ?? '', [
        'class' => 'error',
        'style' => 'color:red;padding: .2rem;font-size: .5rem;'
    ])
    ->addInput("text", "title", [
        'class' => 'title',
        'value' => htmlentities($request->request->get('title') ?? '')
        ])
    ->endFieldSet()
    // textarea
    ->startFieldSet()
    ->addSpan("span-error", $errors['textarea'] ?? '',[
        'class' => 'error',
        'style' => 'color:red;padding: .2rem;font-size: .5rem;'
    ])
    ->addTextarea("textarea", htmlentities($request->request->get('textarea') ?? ''),  ['id' => 'content',
    'cols' => 30,
    'rows' => 10])
    ->endFieldSet()
    // input file
    ->startFieldSet()
    ->addLabel("category_image", "Image de la catégorie", [])
    ->addSpan("span-error", $errors['image'] ?? '', [
        'class' => 'error',
        'style' => 'color:red;padding: .2rem;font-size: .5rem;'
    ])
    ->addInput("file", "category_image", [
        'class' => 'file',
        'id' => 'image',
        'value' => htmlentities($request->request->get('file') ?? '')
        ])
    ->endFieldSet()
    // submit
    ->startFieldSet()
    ->addButton("Enregistrer", ['type' => 'submit', 'name' => 'submit','class' => 'submit'])
    ->endFieldSet()
    ->endForm();