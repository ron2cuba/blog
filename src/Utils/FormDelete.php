<?php
declare(strict_types=1);

namespace App\Utils;
$routes = include '..\src\routes.php';

$form = new Form();
$form->startForm('post', $slug,[
    'class' => 'form',
    'id' => 'delete-category'
])
->startFieldSet()
->addButton("Supprimmer $title", [
    'type' => 'submit',
    'name' => 'delete',
    'class' => 'delete',
    'style' => 'background-color: red; border-color: red;'  
    ])
->endFieldSet()
->endform();