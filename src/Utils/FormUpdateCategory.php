<?php
declare(strict_types=1);

namespace App\Utils;
$routes = include '../src/routes.php';

$form = new Form();
// $request::createFromGlobals()->query->get('url')
$form->startForm('post', $slug, [
    'class' => 'form',
    'id' => 'createCategoryId'
    ])
    // input title
    ->startFieldSet()
    ->addLabel("title", "Titre", [])
    ->addSpan("span-error", $errors['title'] ?? '', [
        'class' => 'error',
        'style' => 'color:red;padding: .2rem;font-size: .5rem;'
    ])
    ->addInput("text", "title", [
        'class' => 'title',
        'value' => html_entity_decode($categoryToEdit->title ?? '')
        ])
    ->endFieldSet()
    // textarea
    ->startFieldSet()
    ->addSpan("span-error", $errors['textarea'] ?? '',[
        'class' => 'error',
        'style' => 'color:red;padding: .2rem;font-size: .5rem;'
    ])
    ->addTextarea("textarea", html_entity_decode($categoryToEdit->content ?? ''), ['id' => 'content',
    'cols' => 30,
    'rows' => 10])
    ->endFieldSet()
    // input file
    ->startFieldSet()
    ->addLabel("image", "Image existante :", [])
    ->addSpan("span-error", $errors['image'] ?? '', [
        'class' => 'error',
        'style' => 'color:red;padding: .2rem;font-size: .5rem;'
    ])
    ->addDiv('Image enregistrée: ')
    ->addSpan('file-exist', $categoryToEdit->image_category ?? '', [])
    ->addImage("50", $categoryToEdit->image_category)
    ->closeDiv()
    ->addInput("file", "image_category", [
        'class' => 'file',
        'id' => 'image',
        ])
    ->endFieldSet()
    // submit
    ->startFieldSet()
    ->addDiv("",["class" => "grid"])
    ->addButton("Enregistrer", ['type' => 'submit', 'name' => 'submit','class' => 'submit'])
    ->closeDiv()
    ->endFieldSet()
    ->endForm();