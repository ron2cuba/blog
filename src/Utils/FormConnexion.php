<?php
declare(strict_types=1);

namespace App\Utils;
$routes = include '../src/routes.php';

$connexion = new Form();

$connexion->startForm('POST', '', [
    'class' => 'form',
    'id' => 'login',
    'style' => 'position: relative'
])
->addH2('login', 'J\'ai déjà un compte', [])
->startFieldSet()
->addLabel("username", "username*", [])
->addSpan("span-error", $errors['username'] ?? '',[
    'class' => 'error',
    'style' => 'color:red;padding: .2rem;font-size: .5rem;'
])
->addInput("text", "username", [
    'class' => 'username',
    'value' => !empty($request->get('usename')) ? htmlentities($request->get('username')) : '',
    'required' => ''
    ])
->addLabel("password", "password*", [])
->addSpan("span-error", $errors['password'] ?? '',[
    'class' => 'error',
    'style' => 'color:red;padding: .2rem;font-size: .5rem;'
])
->addInput('password', 'password', ['required' => ''])
->endFieldSet()
->startFieldSet()
->addButton("Se connecter", [
    'type' => 'submit',
    'name' => 'submit',
    'class' => 'submit'
    ])
->endFieldSet()
->endForm();