<?php
declare(strict_types=1);

namespace App\Utils;
$routes = include '../src/routes.php';

$form = new Form();

$form->startForm('post', $routes->get('contact')->getPath(), [
    'class'=>'form'
])
// nom
->startFieldSet()
->addLabel('nom', 'votre nom', [])
->addSpan("span-error", $errors['nom'] ?? '', [
    'class' => 'error',
    'style' => 'color:red;padding: .2rem;font-size: .5rem;'
])
->addInput("text", "name", [
    'class' => 'name',
    'value' => htmlentities($request->get('name') ?? ''),
    'placeholder' => 'saisir votre nom'
    ])
->endFieldSet()
// prénom
->startFieldSet()
->addLabel('first_name', 'votre prénom', [])
->addSpan("span-error", $errors['first_name'] ?? '', [
    'class' => 'error',
    'style' => 'color:red;padding: .2rem;font-size: .5rem;'
])
->addInput("text", "first_name", [
    'class' => 'first_name',
    'value' => htmlentities($request->request->get('first_name') ?? ''),
    'placeholder' => 'saisir votre prénom'
    ])
->endFieldSet()
// email
->startFieldSet()
->addLabel('email', 'votre email', [])
->addSpan("span-error", $errors['email'] ?? '', [
    'class' => 'error',
    'style' => 'color:red;padding: .2rem;font-size: .5rem;'
])
->addInput("email", "email", [
    'class' => 'first_name',
    'value' => htmlentities($request->request->get('email') ?? ''),
    'placeholder' => 'saisir votre email'
    ])
->endFieldSet()
// textarea
->startFieldSet()
->addLabel('textarea', 'votre message', [])
->addSpan("span-error", $errors['textarea'] ?? '', [
    'class' => 'error',
    'style' => 'color:red;padding: .2rem;font-size: .5rem;'
])
->addTextarea('textarea', $request->request->get('textarea') ?? '', [
    'placeholder'=>'saisir votre message'
])
->endFieldSet()
// submit
->startFieldSet()
->addButton("Envoyer", ['type' => 'submit', 'name' => 'submit','class' => 'submit'])
->endFieldSet()
->endForm();