<?php
declare(strict_types=1);

namespace App\Utils;

class Form
{
    private $htmlForm;
    /**
     * Generate HtmlForm code
     */
    public function generate()
    {
        return $this->htmlForm;
    }

    /**
     * Generate attributes for HTML tag
     *
     * @param array $attributes
     * @return string generate attributes
     */
    private function addAttributes(array $attributes):string
    {
        $str = "";

        $shortAttr = [
            'checked',
            'disabled',
            'readonly',
            'multiple',
            'required',
            'autofocus',
            'novalidate',
            'formnavalidate'
        ];

        foreach($attributes as $attribute => $valueA){
            if (in_array($attribute, $shortAttr) && $valueA == true) {
                $str .= " $attribute";
            } else {
                $str .= " $attribute='$valueA'";
            }
        }

        return $str;
    }

    /**
     * Generate Starting form tag
     *
     * @param string $method form method
     * @param string $action form action
     * @param array $attributes
     * @return Form
     */
    public function startForm(
        string $method = 'post',
        string $action = '#',
        array $attributes = []):self
    {
        $this->htmlForm .= "<form action='$action' method='$method' enctype='multipart/form-data'";

        $this->htmlForm .= $attributes ? $this->addAttributes($attributes).'>' : '>';

        return $this;
    }

    /**
     * Generate Ending form tag
     *
     * @return Form
     */
    public function endForm():self
    {
        $this->htmlForm .= "</form>";
        return $this;
    }

    /**
     * generate start fielset field
     *
     * @return self
     */
    public function startFieldSet(): self
    {
        $this->htmlForm .= "<fieldset>";
        return $this;
    }

    /**
     * close fielset field
     *
     * @return self
     */
    public function endFieldSet(): self
    {
        $this->htmlForm .= "</fieldset>";
        return $this;
    }

    /**
     * Generate Label tag
     *
     * @param string $for "for" value in form
     * @param string $text text between tag
     * @param array $attributes ex class, id
     * @return Form
     */
    public function addLabel(string $for, string $text, array $attributes = []): self
    {
        $this->htmlForm .= "<label for='$for'";
        $this->htmlForm .= $attributes ? $this->addAttributes($attributes) : '';
        $this->htmlForm .= ">$text</label>";
        return $this;
    }

    /**
     * Generate Input tag
     *
     * @param string $type type of input
     * @param string $name name of input
     * @param array $attributes class, id...
     * @return self
     */
    public function addInput(string $type, string $name, array $attributes = []):self
    {
        $this->htmlForm .= "<input type='$type' name='$name'";
        $this->htmlForm .= $attributes ? $this->addAttributes($attributes).'>' : '>';
        return $this;
    }

    /**
     * Generate Span tag
     *
     * @param string $id id for span
     * @param array $attributes class, id
     * @return self
     */
    public function addSpan(string $id, string $text, array $attributes):self
    {
        $this->htmlForm .= "<span id='$id'";
        $this->htmlForm .= $attributes ? $this->addAttributes($attributes).'>'.$text.'</span>' : '>'.$text.'</span>';
        return $this;
    }

    /**
     * Generate Textarea tag
     *
     * @param string $name name of textarea
     * @param string $value value between tags
     * @param array $attributes class, id...
     * @return self
     */
    public function addTextarea(string $name, string $value = '', array $attributes = []):self
    {
        $this->htmlForm .= "<textarea name='$name'";
        $this->htmlForm .= $attributes ? $this->addAttributes($attributes) : '';
        $this->htmlForm .= ">$value</textarea>";
        return $this;
    }

    /**
     * Generate Select tag
     *
     * @param object $object
     * @param string $nom
     * @param array $options
     * @param array $attributes
     * @return self
     */
    public function addSelect(string $nom, ?string $default, array $options, array $attributes = []):self
    {
        //
        $this->htmlForm .= "<select name='$nom'";
        $this->htmlForm .= $attributes ? $this->addAttributes($attributes) .'>' : '>';
        foreach ($options as $value) {
            if ($value->title === $default) {
            $this->htmlForm .= "<option value='$value->id' selected>$value->title</option>";
            } else {
                $this->htmlForm .= "<option value='$value->id'>$value->title</option>";
            }
        }
        $this->htmlForm .= "</select>";
        return $this;
    }
    
    /**
     * Generate CheckBox
     *
     * @param string $id
     * @param string $name
     * @param array $attributes
     * @return self
     */
    public function addCheckBox(string $id, string $name, array $attributes = []): self
    {
        $this->htmlForm .= "<input type='checkbox' id='$id' name='$name'";
        $this->htmlForm .= $attributes ? $this->addAttributes($attributes) .'>' : '>';
        return $this;
    }

    /**
     * open and close html h2 tags
     *
     * @param string $id
     * @param string $text
     * @param array $attributes
     * @return self
     */
    public function addH2(string $id, string $text, array $attributes = []):self
    {
        $this->htmlForm .= '<h2>'. $text .'</h2>';
        return $this;
    }

    /**
     * Generate Div tag
     * 
     * @param array $attributes
     * @return self
     */
    public function addDiv(string $text = "", array $attributes = []):self
    {
        $this->htmlForm .= "<div";
        $this->htmlForm .= $attributes ? $this->addAttributes($attributes) .'>'.$text : '>'.$text;
        return $this;
    }

    /**
     * Generate html image tag
     *
     * @param string $size
     * @param string $src
     * @return self
     */
    public function addImage(string $size, string $src): self
    {
        $this->htmlForm .= "<img width='{$size}px' src='/../../public/uploads/$src'>";
        return $this;
    }

    /**
     * close div tag
     *
     * @return self
     */
    public function closeDiv():self
    {
        $this->htmlForm .= "</div>";
        return $this;
    }

    /**
     * Generate Button
     *
     * @param string $text text in button
     * @param array $attributes class, id...
     * @return self
     */
    public function addButton(string $text, array $attributes = []):self
    {
        $this->htmlForm .= "<button";
        $this->htmlForm .= $attributes ? $this->addAttributes($attributes) : '';
        $this->htmlForm .= ">$text</button>";
        return $this;
    }

}
