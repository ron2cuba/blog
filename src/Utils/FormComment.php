<?php
declare(strict_types=1);

namespace App\Utils;
$routes = include '../src/routes.php';

$form = new Form();

$form->startForm('post', '', [])
// title
->startFieldSet()
->addLabel('title', 'Titre de votre commentaire', ['class'=>'comment-label'])
->addInput("text", "title", [
    "placeholder" => "saisir le titre"
])
->endFieldSet()
// textarea
->startFieldSet()
->addLabel('comment', 'Votre commentaire', ['class'=>'comment-label'])
->addTextarea('comment', '', [
    "placeholder" => "Ecrire le commentaire"
])
->endFieldSet()
// button
->startFieldSet()
->addButton("Envoyer", [
    "type" => "submit"
])
->endFieldSet()
->endForm();