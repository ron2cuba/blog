<?php
declare(strict_types=1);

namespace App\Utils;
$routes = include '../src/routes.php';

$register = new Form();

$register->startForm('POST', $routes->get('register')->getPath(), [
    'class' => 'form',
    'id' => 'login',
    'style' => 'position: relative'
])
->addH2('register', 'Je créer un compte')
->startFieldSet()
->addLabel("email", "email*", [])
->addSpan("span-error", $errors['email'] ?? '',[
    'class' => 'error',
    'style' => 'color:red;padding: .2rem;font-size: .5rem;'
])
->addInput("email", "email", [
    'class' => 'email',
    'value' => !empty($request->get('email')) ? htmlentities($request->get('email')) : '',
    'required' => ''
    ])
->endFieldSet()
// username
->startFieldSet()
->addLabel("username", "username*", [])
->addSpan("span-error", $errors['username'] ?? '',[
    'class' => 'error',
    'style' => 'color:red;padding: .2rem;font-size: .5rem;'
])
->addInput("text", "username", [
    'class' => 'username',
    'value' => $request->get('username') ?? '',
    'required' => ''
    ])
->endFieldSet()
// password
->startFieldSet()
->addLabel("password", "password*", [])
->addSpan("span-error", $errors['password'] ?? '',[
    'class' => 'error',
    'style' => 'color:red;padding: .2rem;font-size: .5rem;'
])
->addInput('password', 'password', [
    'value' => "",
    'required' => ''
    ])
->endFieldSet()
// password repeat
->startFieldSet()
->addLabel("repeat-password", "répéter*", [])
->addSpan("span-error", $errors['correspondence'] ?? '',[
    'class' => 'error',
    'style' => 'color:red;padding: .2rem;font-size: .5rem;'
    ])
->addInput('password', 'repeat-password', ['required' => ''])
->endFieldSet()
// textarea
->startFieldSet()
->addLabel("biography", "biography*", [])
->addSpan("span-error", $errors['textarea'] ?? '',[
    'class' => 'error',
    'style' => 'color:red;padding: .2rem;font-size: .5rem;'
])
->addTextarea("textarea", htmlentities($request->request->get('textarea') ?? ''), ['id' => 'content',
'cols' => 30,
'rows' => 10])
->endFieldSet()
// image
->startFieldSet()
->addLabel('user_image', 'image*', [])
->addSpan("span-error", $errors['image'] ?? '',[
    'class' => 'error',
    'style' => 'color:red;padding: .2rem;font-size: .5rem;'
    ])
->addInput('file', 'user_image', [
    "required" => ""
])
->endFieldSet()
// button
->startFieldSet()
->addButton("Se connecter", ['type' => 'submit', 'name' => 'submit','class' => 'submit'])
->endFieldSet()
->endForm();