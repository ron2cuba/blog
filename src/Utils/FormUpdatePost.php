<?php
namespace App\Utils;

$routes = include '../src/routes.php';

$form = new Form();
if($admin){
    $form->startForm('post', $slug, [
        'class' => 'form',
        'id' => 'createPostId'
        ])
        // input title
        ->startFieldSet()
        ->addLabel("title", "Titre", [])
        ->addSpan("span-error", $errors['title'] ?? '',[
            'class' => 'error',
            'style' => 'color:red;padding: .2rem;font-size: .5rem;'
        ])
        ->addInput("text", "title", [
            'class' => 'title',
            // 'value' => htmlentities($_POST['title'] ?? '')
            'value' => html_entity_decode($postToEdit->title)
            ])
        ->endFieldSet()
        // Select
        ->startFieldSet()
        ->addLabel("select", "Catégorie")
        ->addSelect("categories", $categoryName, $categories, [
            'class' => 'categorie'
        ])
        ->endFieldSet()
        // textarea
        ->startFieldSet()
        ->addSpan("span-error", $errors['textarea'] ?? '',[
            'class' => 'error',
            'style' => 'color:red;padding: .2rem;font-size: .5rem;'
        ])
        ->addTextarea("textarea", html_entity_decode($postToEdit->content ?? ''), ['id' => 'content',
        'cols' => 30,
        'rows' => 10])
        ->endFieldSet()
        // image
        ->addSpan("span-error", $errors['image'] ?? '',[
            'class' => 'error',
            'style' => 'color:red;padding: .2rem;font-size: .5rem;'
        ])
        ->startFieldSet()
        ->addLabel('image', 'Image d\'illustration* (format: jpg, jpeg, gif, png)', [])
        ->addDiv('Image enregistrée: ')
        ->addImage("50", $postToEdit->post_image)
        ->closeDiv()
        ->addInput("file", "post_image", [
            'id'=>'post_image',
            'class'=>'post_image'
        ])
        ->endFieldSet()
        // checkbox
        ->addLabel("is_published", "Publier", [])
        ->addCheckBox("is_published", "is_published", ['style' => 'margin-bottom: 1rem;'])
        // submit
        ->startFieldSet()
        ->addButton("Enregistrer", ['type' => 'submit', 'name' => 'submit','class' => 'submit'])
        ->endFieldSet()
        ->endForm();
} else {
    $form->startForm('post', $slug, [
    'class' => 'form',
    'id' => 'createPostId'
    ])
    // input title
    ->startFieldSet()
    ->addLabel("title", "Titre", [])
    ->addSpan("span-error", $errors['title'] ?? '',[
        'class' => 'error',
        'style' => 'color:red;padding: .2rem;font-size: .5rem;'
    ])
    ->addInput("text", "title", [
        'class' => 'title',
        // 'value' => htmlentities($_POST['title'] ?? '')
        'value' => html_entity_decode($postToEdit->title)
        ])
    ->endFieldSet()
    // Select
    ->startFieldSet()
    ->addLabel("select", "Catégorie")
    ->addSelect("categories", $categoryName, $categories, [
        'class' => 'categorie'
    ])
    ->endFieldSet()
    // textarea
    ->startFieldSet()
    ->addSpan("span-error", $errors['textarea'] ?? '',[
        'class' => 'error',
        'style' => 'color:red;padding: .2rem;font-size: .5rem;'
    ])
    ->addTextarea("textarea", html_entity_decode($postToEdit->content ?? ''), ['id' => 'content',
    'cols' => 30,
    'rows' => 10])
    ->endFieldSet()
    // image
    ->addSpan("span-error", $errors['image'] ?? '',[
        'class' => 'error',
        'style' => 'color:red;padding: .2rem;font-size: .5rem;'
    ])
    ->startFieldSet()
    ->addLabel('image', 'Image d\'illustration* (format: jpg, jpeg, gif, png)', [])
    ->addDiv('Image enregistrée: ')
    ->addImage("50", $postToEdit->post_image)
    ->closeDiv()
    ->addInput("file", "post_image", [
        'id'=>'post_image',
        'class'=>'post_image'
    ])
    ->endFieldSet()
    // submit
    ->startFieldSet()
    ->addButton("Enregistrer", ['type' => 'submit', 'name' => 'submit','class' => 'submit'])
    ->endFieldSet()
    ->endForm();
}

