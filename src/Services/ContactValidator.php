<?php
declare(strict_types=1);

namespace App\Services;

use Helpers\HelperMethods;
use Symfony\Component\HttpFoundation\Request;

class ContactValidator {
    
    private $_name;
    private $_firstName;
    private $_email;
    private $_content;

    public function __construct(public Request $request)
    {
        $this->_name = htmlentities($request->request->get('name'));
        $this->_firstName = htmlentities($request->request->get('first_name'));
        $this->_email = htmlentities($request->request->get('email'));
        $this->_content = htmlentities($request->request->get('textarea'));
        $this->checkName();
        $this->checkFirstName();
        $this->checkEmail();
        $this->checkContent();
    }

    /**
     * check if value in email field exists
     *
     * @return void
     */
    private function checkEmail(): void
    {
        if (empty($this->_email)) {
            $this->hasError('email', 'un email est obligatoire');
            HelperMethods::notify('un email est obligatoire', "#FF0000", "#fff");
        }
        HelperMethods::validateEmail($this->_email);
    }

    /**
     * check if value in name field exists
     *
     * @return void
     */
    private function checkName():void
    {
        if (empty($this->_name)) {
            $this->hasError('name', 'le nom est obligatoire');
            HelperMethods::notify('le nom est obligatoire', "#FF0000", "#fff");
        }
    }

    /**
     * check if value in firstname field exists
     *
     * @return void
     */
    private function checkFirstName()
    {
        if (empty($this->_firstName)) {
            $this->hasError('first-name', 'le prénom est obligatoire');
            HelperMethods::notify('le prénom est obligatoire', "#FF0000", "#fff");
        }
    }

    /**
     * check if value in textarea field exists
     *
     * @return void
     */
    public function checkContent()
    {
        if(empty($this->_content)){
            $this->hasError('content', 'le contenu est obligatoire');
            HelperMethods::notify('le contenu est obligatoire', "#FF0000", "#fff");
        }
    }

    /**
     * add error in errors array
     *
     * @param [type] $fieldInError
     * @param [type] $valueForError
     * @return void
     */
    public function hasError($fieldInError, $valueForError)
    {
        $this->_errors[$fieldInError] = $valueForError;
    }

    /**
     * Get the value of _name
     */
    public function getName()
    {
        return $this->_name;
    }
    
    /**
     * Get the value of _firstName
     */
    public function getFirstName()
    {
        return $this->_firstName;
    }

    /**
     * Get the value of _email
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * Get the value of _content
     */
    public function getContent()
    {
        return $this->_content;
    }

    /**
     * Getter for errors
     *
     * @return array
     */
    public function getErrors(): array
    {
        return $this->_errors ?? [];
    }

    /**
     * method to send email
     *
     * @return void
     */
    public function sendEmail()
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $from = $this->_name.' '.$this->_firstName.' '.$this->_email;
        $to = 'ronan.lenouvel@gmail.com';
        $subject = "Contact blog - $this->_firstName $this->_name $this->_email";
        $message = html_entity_decode($this->_content, ENT_QUOTES, 'utf-8');
        $headers = 'From ' . $from;
        return mail($to, $subject, $message, $headers);
    }

}
