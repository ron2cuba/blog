<?php
declare(strict_types=1);

namespace App\Services;

use Helpers\HelperMethods;
use Symfony\Component\HttpFoundation\Request;

class UserValidator extends Validator
{
    private $_username;
    private $_hasPassword;
    private $_slug;
    private $_content;
    private $_createdAt;
    private $_email;
    private $_role;
    private $_id;

    public function __construct(public Request $request)
    {        
        $this->_id = $request->request->get('user_id');
        // determine process to execute
        switch ($request->attributes->get('_route')) {
            case $request->attributes->get('_route') === 'register':
                $this->registerProcess();
                break;
            case $request->attributes->get('_route') === 'updateUser':
                $this->updateUserProcess();
                break;
        }
    }

    /**
     * process for register
     *
     * @param Request $request
     * @return void
     */
    private function registerProcess()
    {
        // check & set valid email
        if ($this->checkEmail()) $this->setEmail();

        //check & set sanitize username and slug
        if ($this->checkUsername()) {
            $this->setUserName();
            $this->setSlug();
        }
        // check & set sanitize textarea
        if ($this->checkTextarea()) $this->setContent();

        // check pass length & equality
        if ($this->arePassEqual() && $this->passLength()){
            $this->encodePassword();
        }
        $this->setRole();

        $this->setCreatedAt();
    }

    /**
     * Process for connexion
     *
     * @return void
     */
    private function updateUserProcess()
    {
        // process with update password
        if ($this->request->request->get('confirm-actual')) {
            
            if ($this->confirmOldPass()) {
                $this->updatePassword();

                if ($this->checkEmail()){
                 $this->setEmail();
                }
                if ($this->checkUsername()){
                    $this->setUserName();
                }
                
                if ($this->checkTextarea()){
                    $this->setContent();
                }

                $this->checkRoleFromRequest();

                $this->checkCreatedAt();

                $this->setSlug();
            }
        }

        // process without update pass
        $this->getActualPasswordFromRequest();
        
        if ($this->checkEmail()) $this->setEmail();

        if ($this->checkUsername()) $this->setUserName();
        
        if ($this->checkTextarea()) $this->setContent();

        $this->checkRoleFromRequest();

        $this->checkCreatedAt();

        $this->setSlug();
}

    /**
     * Undocumented function
     *
     * @param [type] $password
     * @return bool
     */
    private function passLength(): bool
    {
        if(!strlen($this->request->get('password')) > 7){
            HelperMethods::notify('mot de passe 8 char requis', "#FF0000", "#fff");
            $this->hasError('password', 'mot de passe 8 char requis');
            return false;
        }
        return true;
    }

    /**
     * Encode password
     *
     * @return string
     */
    private function encodePassword():string
    {
        return $this->_hasPassword = password_hash($this->_sanitize($this->request->request->get('password')), PASSWORD_BCRYPT,[
            'cost' => 9
        ]);
    }

    /**
     * check if password is equal to confirm
     *
     * @return bool
     */
    private function arePassEqual():bool
    {
        $message = 'le mot de passe et sa validation doivent correspondre';
        
        if ($this->request->get('password') === $this->request->get('repeat-password')){
            return true;
        }
        HelperMethods::notify($message, 'red', '#fff');
        return false;
    }

    /**
     * confirm old password if user want to change
     *
     * @return bool
     */
    private function confirmOldPass(): bool
    {
        if (password_verify($this->request->get('confirm-actual'), $this->getActualPasswordFromRequest())){
            return true;
        }
        return false;
    }

    private function updatePassword()
    {
        if ($this->passLength()) {
            if($this->arePassEqual()) $this->encodePassword();
        }
    }

    /**
     * Sanitize passed text
     *
     * @param string $text
     * @return string
     */
    private function _sanitize(string $text):string
    {
        return htmlentities(trim($text));
    }

    /**
     * Check username validity
     */
    private function checkUsername()
    {
        if(empty($this->request->request->get('username'))){
            HelperMethods::notify('username est un champ obligatoire', "#FF0000", "#fff");
            return $this->hasError('username', 'username est obligatoire');
        }
        return true;
    }

    /**
     * set current password from request in validator during update process
     *
     * @return string
     */
    private function getActualPasswordFromRequest():string
    {
        return $this->_actualPassword =$this->request->request->get('actual_password');
    }

    /**
     * set Role in validator from request
     *
     * @return string
     */
    private function checkRoleFromRequest(): string
    {
        return $this->_role = $this->request->request->get('role');
    }

    /**
     * Set unique role Subscriber
     *
     * @return string
     */
    private function setRole(): string
    {
        return $this->_role = "Subscriber";
    }
    
    /**
     * Set email in validator from Request
     *
     * @return string
     */
    private function setEmail():string
    {
        return $this->_email = $this->request->request->get('email');
    }
    
    /**
     * Set sanitized username in validator from request
     *
     * @return string
     */
    private function setUserName(): string
    {
        return $this->_username = $this->_sanitize($this->request->request->get('username'));
    }
    
    /**
     * set slug in validator based on the username
     *
     * @return string
     */
    private function setSlug(): string
    {
        return $this->_slug =  HelperMethods::slugify($this->_username);
    }
    
    /**
     * set created_at in validator
     *
     * @return string
     */
    private function setCreatedAt(): string
    {
        return $this->_createdAt = date('Y-m-d H:i:s');
    }
    
    /**
     * set created_at in validator from requets during update
     *
     * @r
     */
    private function checkCreatedAt()
    {
        return $this->_createdAt = $this->request->request->get('created_at');
    }

    /**
     * Check if textarea is filled
     *
     * @return void
     */
    private function checkTextarea()
    {
        if(empty($this->request->get('textarea'))){
            HelperMethods::notify('textarea est un champ obligatoire', "#FF0000", "#fff");
            $this->hasError('textarea', 'textarea est obligatoire');
            return false;
        }

        return true;
    }

    /**
     * set cxontent in validator from request
     *
     * @return string
     */
    private function setContent(): string
    {
        return $this->_content = $this->_sanitize($this->request->request->get('textarea'));
    }

    /**
     * check if email field is valid
     *
     * @return bool
     */
    private function checkEmail(): bool
    {
        if(!filter_var($this->request->get('email'), FILTER_VALIDATE_EMAIL)){
            HelperMethods::notify('email non valide', "#FF0000", "#fff");
            $this->hasError('email', 'email non valide');
            return false;
        }
        return true;
    }

    /**
     * add error in errors array
     *
     * @param [type] $fieldInError
     * @param [type] $valueForError
     * @return void
     */
    public function hasError($fieldInError, $valueForError)
    {
        $this->_errors[$fieldInError] = $valueForError;
    }

    /**
     * Getter for errors useful in controller
     *
     * @return array
     */
    public function getErrors(): array
    {
        return $this->_errors ?? [];
    }
    
    /**
     * get hashed password useful in prepareUser
     *
     * 
     */
    public function getHashedPassword()
    {
        return $this->_hasPassword;
    }

    /**
     * get email useful in controller
     *
     * @return string
     */
    public function getEmail():string
    {
        return $this->_sanitize($this->_email);
    }

    /**
     * get username useful in controller
     *
     * @return string
     */
    public function getUsername():string
    {
        return $this->_username;
    }

    /**
     * get Role useful in controller
     *
     * @return string
     */
    public function getRole():string
    {
        return $this->_role;
    }

    /**
     * get Content useful in controller
     *
     * @return string
     */
    public function getContent():string
    {
        return $this->_content;
    }

    /**
     * get CreatedAt useful in controller
     *
     * @return string
     */
    public function getCreatedAt():string
    {
        return $this->_createdAt;
    }


    /**
     * get slug  useful in controller
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->_slug;
    }

    public function getid()
    {
        return $this->_id;
    }
}