<?php
declare(strict_types=1);

namespace App\Services;

use App\Database\UserModel;
use PDOException;
use Symfony\Component\HttpFoundation\Request;

class UserExists
{
    private $_username;
    public $_model;
    public $request;
    
    public function __construct(Request $request, UserModel $model)
    {
        $this->_username = htmlentities($request->get('username'));
        $this->_password = htmlentities($request->get('password'));
        $this->_model = $model;
    }

    public function getRole()
    {
        return $this->_model->role;
    }

    /**
     * check if user exist in database
     *
     * 
     */
    public function presentInDatabase()
    {
        if($this->_model->findOneBy(['username' => $this->_username])){
            $this->_model = $this->_model->findOneBy(['username' => $this->_username]);
            return true;  
        }
        return false;
    }

    public function isAdmin()
    {
        dd($this->_model->role);
    }

    /**
     * Get the value of email
     */
    public function getEmail()
    {
        return $this->_username;
    }

    /**
     * Get the value of _model
     */
    public function getModel()
    {
        return $this->_model;
    }
}