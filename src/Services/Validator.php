<?php
declare(strict_types=1);

namespace App\Services;

use Helpers\HelperMethods;
use Symfony\Component\HttpFoundation\Request;

class Validator
{
    protected $_post_id;
    protected $_user_id;
    protected $_category_id;
    protected $_originalCategoryId;
    protected $_sanitizeTitle;
    protected $_sanitizeTextarea;
    protected $_published;
    protected $_updatedAt;
    protected $_post_owner;

    public function __construct(public Request $request)
    {
        switch ($request->attributes->get('_route')) {
            case $request->attributes->get('_route') === 'createPost':
                $this->createProcess();
                break;
            case $request->attributes->get('_route') === 'updatePost':
                $this->updateProcess();
                break;
            case $request->attributes->get('_route') === 'createCategory':
                $this->createProcess();
                break;
            case $request->attributes->get('_route') === 'updateCategory':
                $this->updateProcess();
                break;
        }

    }

    /**
     * add error in errors array
     *
     * @param [type] $fieldInError
     * @param [type] $valueForError
     * @return void
     */
    public function hasError($fieldInError, $valueForError)
    {
        $this->_errors[$fieldInError] = $valueForError;
    }

    /**
     * create a post
     *
     * @return void
     */
    protected function createProcess()
    {
        $this->_validateTitle();
        $this->_validateTextarea();
        $this->_is_published($this->request);
        $this->_setCreatedAt();
    }

    protected function _is_published(Request $request)
    {
        if ($request->get('is_published') === 'on') {
            $this->_published = 1;
        }
    }

    /**
     * update a post
     *
     * @return void
     */
    protected function updateProcess()
    {
        $this->_validateTitle();
        $this->_validateTextarea();
        $this->_is_published($this->request);
        $this->_updatedAt = $this->_initializeDate();
        $this->_originalCategoryId = $this->request->attributes->get('originalCategoryId');
        $this->_category_id = $this->request->get('categories');
        $this->_post_id = $this->request->attributes->get('post_id');
        $this->_post_owner = $this->request->attributes->get('post_owner');
    }

    /**
     * sanitize title or set error
     *
     * @return void
     */
    private function _validateTitle():void
    {
        $this->_sanitizeTitle = $this->_sanitize($this->request->request->get('title'));
        $this->request->request->set('title', $this->_sanitizeTitle);
        if(empty($this->_sanitizeTitle)){
            $this->hasError('title', 'titre est un champ obligatoire');
            HelperMethods::notify('titre est un champ obligatoire', "#FF0000", "#fff");
        }
    }

    /**
     * prepare data for textarea
     *
     * @return void
     */
    private function _validateTextarea():void
    {
        $this->request->request->set('textarea', $this->_sanitize($this->request->request->get('textarea')));
        $this->_sanitizeTextarea = $this->_sanitize($this->request->request->get('textarea'));
        if(empty($this->getSanitizedTextarea())){
            $this->hasError('textarea', 'une description est obligatoire');
            HelperMethods::notify('description est un champ obligatoire', "#FF0000", "#fff");
        }
    }

    public function _initializeDate()
    {
        return date('Y-m-d H:i:s');
    }

    public function getUpdatedAt()
    {
        return $this->_updatedAt;
    }

    public function _setCreatedAt()    {
        return $this->_createdAt = date('Y-m-d H:i:s');
    }

    public function getCreatedAt()
    {
        return $this->_createdAt;
    }

    public function getPostId()
    {
        return $this->_post_id;
    }
    public function getUserId()
    {
        return $this->_user_id;
    }

    public function getCategoryId()
    {
        return $this->_category_id;
    }

    public function getOriginalCategoryId()
    {
        return $this->_originalCategoryId;
    }

    
    /**
     * Sanitize pased text
     *
     * @param string $text
     * @return string
     */
    private function _sanitize(string $text):string
    {
        return htmlentities(trim($text));
    }
    
    /**
     * Getter for errors
     *
     * @return array
     */
    public function getErrors(): array
    {
        return $this->_errors ?? [];
    }
    
    /**
     * getter for sanitized title
     *
     * @return string
     */
    public function getSanitizedTitle(): string
    {
        return $this->_sanitizeTitle;
    }

    /**
     * Get sanitize textarea content 
     *
     * 
     */
    public function getSanitizedTextarea()
    {
        return $this->request->request->get('textarea');
    }
    
    /**
     * getter for is_published
     *
     * @return integer
     */
    public function getIsPublished():int
    {
        return $this->_published ?? 0;
    }

    /**
     * Get the value of _post_owner
     */
    public function getPostOwner()
    {
        return $this->_post_owner;
    }
}