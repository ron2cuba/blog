<?php
declare(strict_types=1);

namespace App\Services;

use Helpers\HelperMethods;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class FileUploader 
{
    private $_fileName;

    public function __construct(private string $_path, private UploadedFile $_file, private Validator $_validator
    )
    {
        if($this->checkImage($this->_file)){
            try {
                $this->_file->move($this->getTargetDirectory(), $this->_fileName);
            } catch (FileException $e) {
                throw new FileException($e->getMessage(), 500);
            }
        }
    }

    /**
     * Get the value of _filename
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->_fileName;
    }

    /**
     * Get the value of _path
     *
     * @return string
     */
    private function getTargetDirectory()
    {
        return $this->_path;
    }

    /**
     * check file extension
     *
     * @param UploadedFile $file
     * @return void
     */
    public function validateFormat($file)
    {
        if(!HelperMethods::validateImageFormat($file)) $this->_validator->hasError('image', 'type de fichier non acepté');
    }

    /**
     * check file size
     *
     * @param UploadedFile $file
     * @return void
     */
    public function validateSize($file)
    {
        return HelperMethods::validateSize($file);
    }

    /**
     * veerification on image
     *
     * @param UploadedFile $file
     * @return bool
     */
    public function checkImage($file)
    {
        $this->validateFormat($file);
        $this->validateSize($file);
        $this->setFileName($file);

        return true;
    }

    /**
     * set filename (hashed)
     *
     * @param UploadedFile $file
     * @return void
     */
    private function setFileName($file)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = HelperMethods::slugify($originalFilename);
        $this->_fileName = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();
    }

}