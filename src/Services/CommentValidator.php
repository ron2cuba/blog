<?php
declare(strict_types=1);

namespace App\Services;

use Faker\Extension\Helper;
use Helpers\HelperMethods;
use Symfony\Component\HttpFoundation\Request;

class CommentValidator
{
    private $_userId;
    private $_title;
    private $_content;
    private $_createdAt;
    private $_isPublished;
    private $_postId;
    
    public function __construct(private Request $_request, int $postId, int $_userId)
    {
        $this->_userId = $_userId;
        $this->_title = htmlentities($_request->get('title'));
        $this->_content = htmlentities($_request->get('comment'));
        $this->_createdAt = $this->setCreatedAt();
        $this->_isPublished = 0;
        $this->_postId = $postId;
    }

    /**
     * add error in errors array
     *
     * @param [type] $fieldInError
     * @param [type] $valueForError
     * @return void
     */
    public function hasError($fieldInError, $valueForError)
    {
        $this->_errors[$fieldInError] = $valueForError;
    }
    
    public function isComplete()
    {
        if (empty($this->_title)) {
            $this->hasError('title', 'le titre est obligatoire');
            HelperMethods::notify('le titre est obligatoire', 'red', '#fff');
        }
        if (empty($this->_content)) {
           $this->hasError('content', 'le contenu du  mlessage est obligatoire');
           HelperMethods::notify('le contenu est obligatoire', 'red', '#fff');
        }
    }

    /**
     * set value for created_at
     */
    public function setCreatedAt()
    {
        return date("Y-m-d H:i:s");
    }

    /**
     * Get the value of _content
     */
    public function getContent()
    {
        return $this->_content;
    }

    /**
     * Get the value of _createdAt
     */
    public function getCreatedAt()
    {
        return $this->_createdAt;
    }

    /**
     * Get the value of _title
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Get the value of _isPublished
     */
    public function getIsPublished()
    {
        return $this->_isPublished;
    }

    /**
     * Get the value of _postId
     */
    public function getPostId()
    {
        return $this->_postId;
    }

    /**
     * Get the value of _userId
     */
    public function getUserId()
    {
        return $this->_userId;
    }
}
