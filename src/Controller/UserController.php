<?php
declare(strict_types=1);
namespace App\Controller;

use Exception;
use PDOException;
use Core\Controller;
use Helpers\HelperMethods;
use App\Database\UserModel;
use App\Services\FileUploader;
use App\Database\CategoryModel;
use App\Database\CommentModel;
use App\Database\PostModel;
use App\Services\UserExists;
use App\Services\UserValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;

class UserController extends Controller
{
    public function __construct(public CategoryModel $model = new CategoryModel())
    {
        $this->categoriesCollection =  $this->model->findAll();
    }
    
    public function register(Request $request)
    {
        // title for template
        $title = "Enregistrement";

        // existing categories
        $categoriesCollection = $this->categoriesCollection;

        $image = $request->get('user_image');

        $path = __DIR__ . '\..\..\public\uploads';        

        // form submission
        if($request->isMethod('POST')){            

            // path for upload
            $path = __DIR__ . '\..\..\public\uploads';
            // image field
            $image = $request->files->get('user_image');
            // validator instanciation
            $validation = new UserValidator($request);
            if(empty($image)){
                $validation->hasError('image', 'une image est requise');
                // notify user
                HelperMethods::notify('une image est requise', "#FF0000", "#fff");
            }
            // get errors
            $errors = $validation->getErrors();

            if (empty($errors)) {
                
                $userModel = new UserModel();
                
                $fileUploader = new FileUploader($path, $image, $validation);

                $settedUser = $this->prepareUser($validation, $userModel, $fileUploader->getFileName());

                try {
                    $userModel->create($settedUser);
                    HelperMethods::notify("Enregistrement effectué", "#3dff3d", "#fff");
                    return new RedirectResponse("/user/" . $validation->getSlug());
                } catch (Exception $e) {
                    return new Response($e->getMessage(), 500);
                }
            }
        }

        /**
         * create Form
         */
        include __DIR__ . '/../Utils/FormRegister.php';

        return new Response($this->view(
            "layouts/handle-connection.html.twig",[
                'title' => $title,
                'form' => $register->generate(),
            ]
        ));
    }

    public function logIn(Request $request)
    {
        $title = 'Se connecter';
        
        $categoriesCollection = $this->categoriesCollection;
        /**
         * create Form
         */
        include __DIR__ . '/../Utils/FormConnexion.php';
        
        if ($request->isMethod('post')) {
            $userModel = new UserModel();
            $userExist = new UserExists($request, $userModel);
            if($userExist->presentInDatabase()){
                // verify password
                if(password_verify(htmlentities($request->get('password')), $userExist->getModel()->password)){
                    // set session data
                    $session = new Session();
                    $session->start();
                    $session->set('name', $userExist->getModel()->username);
                    $session->set('user_id', $userExist->getModel()->id);

                    // is admin
                    $userModel = new UserModel();
                    $adminArray = [];
                    $adminUsers = $userModel->findBy(['role'=> 'Admin']);
                                        
                    foreach ($adminUsers as $admin) {
                        array_push($adminArray, $admin->username);
                    }
                    
                    if (in_array($session->get('name'), $adminArray)) {
                        // get admin data    
                        $admin = $userModel->findOneBy(['username' => $session->get('name')]);
                        return new Response($this->view(
                            'layouts/success.html.twig', [
                                'message' => HelperMethods::notify('bonjour '. $admin->username . ', vous êtes désormais connecté', "#3dff3d", "#fff"),
                                'categoriesCollection' => $this->categoriesCollection,
                                'connectedUser' => $admin,
                            ]
                        ), 200);
                    }

                    $user = $userModel->findOneBy(['username' => $userExist->getModel()->username]);

                    return new Response($this->view(
                        'layouts/success.html.twig', [
                            'categoriesCollection' => $this->categoriesCollection,
                            'connectedUser' => $user,
                            'message' => HelperMethods::notify('bonjour '.$user->username . ', vous êtes désormais connecté', "#3dff3d", "#fff"),
                            // for navbar
                            'connectedUser' => $user
                        ]
                    ), 200);
                }

                return new Response( $this->view(
                    '404/404.html.twig',[
                        'message' => 'Le mot de passe n\'est pas correct'
                    ]
                ), 401);
            }

            return new Response( $this->view(
                '404/404.html.twig',[
                    'message' => 'l\'utilisateur n\'existe pas'
                ]
            ), 401);

            
                
        }
        // if (condition) {
        //     # code...
        // }

        return new Response(($this->view(
            "layouts/handle-connection.html.twig", [
                'categoriesCollection' => $categoriesCollection,
                'connectedUser' => '$connectedUser',
                'title' => $title,
                'form'  => $connexion->generate()
            ]
        )));
    }

    public function admin($model = new UserModel())
    {
        // check all admin
        $users = $model->findBy(['role' => 'Admin']);
        // array to stock them
        $adminArray = [];
        foreach($users as $admin){
            array_push($adminArray, $admin->username);
        }
        // instanciate Session
        $session =  new Session();

        // check connected in admin array
        if (in_array($session->get('name'), $adminArray)) {
            $title = "Administration";
            // pending posts
            $postmodel = new PostModel();
            $pendingPosts = $postmodel->findBy(['is_published' => 0]);
            // category
            $categoryModel = new CategoryModel();
            $categories = $categoryModel->findAll();
            // pending comments
            $commentModel = new CommentModel();
            $pendingComments = $commentModel->findBy(['is_published'=>0]);
            $posts = $postmodel->findAll();
           // response with view admin 
           return new Response($this->view(
                "admin/admin.html.twig",[
                    'title'=> $title,
                    'admin' => $session->get('name'),
                    'pendingPosts' => $pendingPosts,
                    'pendingComments' => $pendingComments,
                    'posts' => $posts,
                    'categories' => $categories
                ]
            ), 200);
        }

        // response for entry on page
        return new Response($this->view(
            '404/404.html.twig',[
                'message' => HelperMethods::notify("Vous n'avez pas les droits", "#FF0000", "#fff")
            ]
        ), 401);
        
    }

    public function logout()
    {
        $session = new Session();
        $session->clear();

        return new Response(
            $this->view(
                "layouts/success.html.twig",[
                    'message'=> HelperMethods::notify('vous êtes désormais déconnecté', "#3dff3d", "#fff")
                ]
            )
        , 200);
    }

    private function prepareUser(UserValidator $validator, UserModel $model, $image): UserModel
    {
        $model->setUsername($validator->getUsername())
        ->setPassword($validator->getHashedPassword())
        ->setSlug($validator->getSlug())
        ->setContent($validator->getContent())
        ->setEmail($validator->getEmail())
        ->setRole($validator->getRole())
        ->setCreatedAt($validator->getCreatedAt())
        ->setUserImage($image);

        return $model;
    }

    public function getUserBySlug(Request $request)
    {
        // get slug from Request
        $slug = $request->attributes->get('slug');
        // instanciate UserMopdel
        $userModel = new UserModel();
        // find user by slug
        if(!$userModel->findOneBy(['slug' => $slug])){
            return new Response($this->view(
                '404/404.html.twig', [
                    'message' => 'l\'utilsateur n\'existe pas'
                ]
            ),404);
        }
        $accesToUser = $userModel->findOneBy(['slug' => $slug]);

        // session
        $session = new Session();
        if ($session->get('name') == $accesToUser->username) {
            $postModel = new PostModel();
            $writtenPosts = $postModel->findBy(['user_id' => $accesToUser->id]);

            return new Response($this->view(
                "layouts/user.html.twig",[
                    'title' => html_entity_decode($accesToUser->username),
                    'image' => html_entity_decode($accesToUser->user_image),
                    'date' => html_entity_decode($accesToUser->created_at),
                    'role' => html_entity_decode($accesToUser->role),
                    'content' => html_entity_decode($accesToUser->content),
                    'slug' => $accesToUser->slug,
                    'writtenPosts' => $writtenPosts
                ]
            ));

        }
        /**
         * create form
         */
        include __DIR__ . '/../Utils/FormUser.php';
        
        if($request->isMethod('post')){
            $validation = new UserValidator($request);
        }

        return new Response($this->view(
            "layouts/user.html.twig",[
                'title' => html_entity_decode($accesToUser->username),
                'image' => html_entity_decode($accesToUser->user_image),
                'date' => html_entity_decode($accesToUser->created_at),
                'role' => html_entity_decode($accesToUser->role),
                'content' => html_entity_decode($accesToUser->content)
            ]
            ));

    }

    public function updateUser(Request $request)
    {
        
        // get user by slug
        $slug = $request->attributes->get('slug');
        $userModel = new UserModel();
        $accesToUser = $userModel->findOneBy(['slug' => $slug]);
        
        // pass user_id in request
        $request->request->set('user_id', $accesToUser->id);
        
        // pass registered user_image in request
        $request->request->set('user_image', $accesToUser->user_image);

        // pass registered content
        $request->request->set('content', $accesToUser->content);
        
        // confirm actual password
        $request->request->set('confirm', $request->request->get('confirm-password'));

        // pass registered hashed password
        $request->request->set('actual_password', $accesToUser->password);
        
        // pass registered email
        $request->request->set('email', $accesToUser->email);
        
        // pass registered role
        $request->request->set('role', $accesToUser->role);
        
        // pass registered username
        $request->request->set('username', $accesToUser->username);

        // pass created_at in request
        $request->request->set('created_at', $accesToUser->created_at);

        // title for twig
        $title = "modification de $accesToUser->username";

        // generate form
        include __DIR__ . '/../Utils/FormUser.php';

        // form submission
        if ($request->isMethod('POST')) {
            // path for uploads
            $path = __DIR__ . '/../../public/uploads';

            // validator instanciation
            $validation = new UserValidator($request);

            // file to upload
            $image = $request->files->get('user_image');

            $errors = $validation->getErrors();

            if (empty($errors)) {
                // model for user
                $userModel = new UserModel();

                if(empty($image)){
                    // set Post data with actual registerd image
                    $image = $accesToUser->user_image;
                    // hydratration
                    $settedUser = $this->prepareUser($validation, $userModel, $image);
                } else {
                    unlink(__DIR__ . "\..\..\public\uploads\\" . $accesToUser->user_image);
                    // image processing and pass it to prepareUser
                    $image = $request->files->get('user_image');
                    $fileUploader = new FileUploader($path, $image, $validation);
                    $settedUser = $this->prepareUser($validation, $userModel, $request->files->get('user_image'));
                }

                if(empty($request->request->get('confirm-actual'))) $settedUser->setPassword($accesToUser->password);

                // prepare data for insertion in database
                $settedUser->id = $validation->getid();
                try {
                    $userModel->update($validation->getId(), $userModel);
                    return new Response($this->view(
                        'layouts/user.html.twig',[
                            'slug' => $validation->getSlug(),
                            'title' => $settedUser->getUsername(),
                            'role' => $settedUser->getRole(),
                            'date' => $settedUser->getCreatedAt(),
                            'image' => $settedUser->getUserImage(),
                            'content' => $settedUser->getContent()
                            ]
                    ) ,200);
                } catch (PDOException $e) {
                    return new Response(
                        $this->view(
                            "404/404.html.twig",[
                                'message'=> $e->getMessage()
                            ]
                        )
                    );
                }
                
            }

            return new Response("user/$slug", 200);

        }

        return new Response($this->view(
            "layouts/create-update-user.html.twig",[
                'title' => $title,
                'image' => $accesToUser->user_image,
                'form' => $form->generate()
            ]
        ));
    }
}