<?php

namespace App\Controller;

use Faker\Factory;
use Helpers\HelperMethods;
use App\Database\PostModel;
use App\Database\UserModel;
use App\Database\CommentModel;
use App\Database\CategoryModel;
use App\Database\PostCategoryModel;
use PDOException;
use Symfony\Component\HttpFoundation\Response;

/**
 * file used at the beginning of the project
 */

class AppFixtures
{
    private $faker;
    private $categoryModel;
    private $userModel;
    private $postModel;
    private $commentModel;
    private $tagModel;

    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');
        $this->categoryModel = new CategoryModel();
        $this->postModel = new PostModel();
        $this->userModel = new UserModel();
        $this->commentModel = new CommentModel();
    }

    public function load()
    {
        $this->loadCategories();
        $this->loadUsers();
        $this->loadPosts();
        $this->loadComments();
        $this->loadTag();
        $this->association();

        return new Response("... process complete", 200);
    }

    public function loadCategories()
    {
        for ($i=0; $i < 5; $i++) {
            $category = $this->categoryModel->setTitle(
                $this->faker->sentence(random_int(1,5))
            )
            ->setSlug(HelperMethods::slugify($this->categoryModel->getTitle()))
            ->setImageCategory("girafe.jpg")
            ->setContent($this->faker->paragraph(random_int(1,3)));
            
            $this->categoryModel->create($category);

        }
        HelperMethods::notify("Categories générées", "#3dff3d", "#fff");
        
    }

    public function loadUsers()
    {
        for ($i=0; $i < 10; $i++) {
            $user = $this->userModel->setUsername($this->faker->userName())
            ->setPassword(password_hash($this->faker->password(), PASSWORD_BCRYPT))
            ->setSlug(HelperMethods::slugify($this->userModel->getUsername()))
            ->setContent($this->faker->paragraph(random_int(1, 2)))
            ->setUserImage("girafe.jpg")
            ->setEmail($this->userModel->getUsername() . "@email.com")
            ->setRole("Subscriber")
            ->setCreatedAt($this->faker->date('Y-m-d', "now") . " " . $this->faker->time('H:i:s'));

            try {
                $this->userModel->create($user);
            } catch (PDOException $e) {
                $e->getMessage();
            }

        }
        /**
         * Admin generation
         */
        $admin = $this->userModel->setUsername('admin')
        ->setPassword(password_hash('admin', PASSWORD_BCRYPT))
        ->setSlug(HelperMethods::slugify($this->userModel->getUsername()))
        ->setContent('Ici dieu c\'est moi !!!!')
        ->setUserImage('girafe.jpg')
        ->setEmail("admin@email.com")
        ->setRole('Admin')
        ->setCreatedAt($this->faker->date('Y-m-d', "now") . " " . $this->faker->time('H:i:s'));

        try {
            $this->userModel->create($user);
        } catch (PDOException $e) {
            $e->getMessage();
        }

        HelperMethods::notify("Users générés", "#3dff3d", "#fff");
        
    }

    public function loadPosts()
    {
        $users = $this->userModel->findAll();

        for ($i=0; $i < 30; $i++) {

            $post = $this->postModel->setTitle($this->faker->sentence(random_int(2,4)))
            
            ->setUserId($this->faker->randomElements($users)[0]->id)
            ->setSlug(HelperMethods::slugify($this->postModel->getTitle()))
            ->setContent($this->faker->paragraph(random_int(10, 20)))
            ->setPostImage("girafe.jpg")
            ->setCreatedAt($this->faker->date('Y-m-d', "now") . " " . $this->faker->time('H:i:s'))
            ->setIsPublished(random_int(0, 1));
            $this->postModel->create($post);
        }
        HelperMethods::notify("Posts - générées", "#3dff3d", "#fff");
        
    }

    public function loadComments()
    {
        for ($i=0; $i < 120; $i++) { 
            $comment = $this->commentModel->setTitle($this->faker->sentence(random_int(1, 3)))
            ->setUserId(random_int(1, 10))
            ->setContent($this->faker->paragraph(random_int(1,3)))
            ->setCreatedAt($this->faker->date('Y-m-d', "now") . " " . $this->faker->time('H:i:s'))
            ->setIsPublished(random_int(0, 1))
            ->setUserId(random_int(0, 10));

            $this->commentModel->create($comment);
        }

        HelperMethods::notify("Commentaires générés", "#3dff3d", "#fff");
    }

    public function loadTag()
    {
        for ($i=0; $i<15; $i++) {
            $tag = $this->tagModel->setTitle($this->faker->word());

            $this->tagModel->create($tag);
        }
    }

    public function association()
    {

        $postCategoryModel = new PostCategoryModel();

        $categories = $this->categoryModel->findAll();
        $tags = $this->tagModel->findAll();
        $posts = $this->postModel;
        
        foreach ($posts->findAll() as $post) {

            // random category
            $assocPostCat = $postCategoryModel->setPostId($post->id)
                ->setCategoryId($this->faker->randomElements($categories)[0]->id);
            $postCategoryModel->create($assocPostCat);
             
        }

        HelperMethods::notify("Associations générées", "#3dff3d", "#fff");
    }
}