<?php
declare(strict_types=1);

namespace App\Controller;

use Exception;
use PDOException;
use Core\Controller;
use Helpers\HelperMethods;
use App\Database\UserModel;
use App\Services\Validator;
use App\Services\FileUploader;
use App\Database\CategoryModel;
use App\Database\PostCategoryModel;
use App\Database\PostModel;
use App\Services\CategoryValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class CategoryController extends Controller
{ 
    public $response;
    private $categoriesCollection;

    public function __construct(public CategoryModel $model = new CategoryModel())
    {
        $this->categoriesCollection =  $this->model->findAll();
        $this->date = date('d-m-Y H:i:s');
    }

    /**
     * creating a category
     *
     * @param Request $request
     * @return Response
     */
    public function createCategory(Request $request) : Response
    {
        // existing categories
        $categoriesCollection = $this->categoriesCollection;
        // check all admin
        $userModel = new UserModel();
        // check all admin
        $users = $userModel->findBy(['role' => 'Admin']);
        // array to stock them
        $adminArray = [];

        // put them in it
        foreach($users as $admin){
            array_push($adminArray, $admin->username);
        }
        // instanciate session
        $session = new Session();
        // check username in session
        if (in_array($session->get('name'), $adminArray)) {
            // title for template
            $title = "création d'une catégorie";

            // form validation        
            if($request->isMethod('POST')){

                // validator instantiation
                $validation = new Validator($request);
                // path for uploads
                $path = __DIR__ . '\..\..\public\uploads';
                // image field
                $image = $request->files->get('category_image');
                // error image
                if(empty($image)){
                    // error in form
                    $validation->hasError('image', 'une image est requise pour illustrer la catégorie');
                    // notify user
                    HelperMethods::notify('une image doit illustrer votre catégorie', "#FF0000", "#fff");
                }
                // get errors
                $errors = $validation->getErrors();

                if(empty($errors)){
                    $categoryModel = new CategoryModel();
                    // image processing
                    $fileUploader = new FileUploader($path, $image, $validation);
                    // pseudo hydration
                    $settedCategory = $this->prepareCategory($validation, $fileUploader->getFileName(), $categoryModel);

                    if($image) {
                        try {
                            $categoryModel->create($settedCategory);
                            return new Response($this->view(
                                "layouts/categories.html.twig",[
                                    'message' => HelperMethods::notify("catégorie crééa avec succès", "#00ff00", "#fff"),
                                    'categoriesColelction' => $categoriesCollection,
                                    'date' => $this->date
                                ]
                            ));
                        } catch (Exception $e) {
                            return new Response($e->getMessage(), 500);
                        }

                    }
                }

            }

            /**
             * create Form
             */
            include __DIR__ . '/../Utils/FormCategory.php';

            /**
             * view with template & data
             */
            return new Response($this->view(
                "layouts/create-update-category.html.twig", [
                    "form" => $form->generate(),
                    "categoriesCollection" => $categoriesCollection,
                    'date' => $this->date
                ]
            ), 200);
                
        }

        /**
         * view with template & data
         */
        return new Response($this->view(
            "404/404.html.twig", [
                "categoriesCollection" => $categoriesCollection,
                'message' => HelperMethods::notify("vous n'avez pas acces à cette page", "red", '#fff'),
                'date' => $this->date
            ]
        ), 200);
    }

    /**
     * set data in category model before persistence
     *
     * @param CategoryValidator $validator
     * @param string $image
     * @param CategoryModel $model
     * @return CategoryModel
     */
    private function prepareCategory(Validator $validator, string $image, CategoryModel $model): CategoryModel
    {
        $model->setTitle($validator->getSanitizedTitle())
        ->setSlug(HelperMethods::slugify($validator->getSanitizedTitle()))
        ->setImageCategory($image)
        ->setContent($validator->getSanitizedTextarea());
        return $model;
    }

    /**
     * List All Categories
     *
     * @return Response
     */
    public function listAllCategories(): Response
    {
        $session = new Session();
        $categoriesCollection = $this->categoriesCollection;

        $title = "Les catégories";

        return new Response($this->view(
            'layouts/categories.html.twig',
            [
                'title' => $title,
                'categoriesCollection' => $categoriesCollection,
                'session' => $session
            ]
        ), 200);
    }

    /**
     * get one category by slug
     *
     * @param Request $request
     * @return Response
     */
    public function getOneCategoryBySlug(Request $request): Response
    {
        // categories Collection
        $categoriesCollection = $this->categoriesCollection;
        // get slug from attribtues
        $slug = $request->attributes->get('slug');
        // data for category
        $cat = $this->model->findOneBy(['slug' => $slug]);
        $catId = $cat->id;
        $name = $cat->title;
        $desc = $cat->content;
        $image = $cat->image_category;
        // postCategoryModel
        $postsCategoryModel = new PostCategoryModel();
        $postModel = new PostModel();
        
        $posts = [];
        // trouver les post avec id venant de l'asso
        foreach ($postsCategoryModel->findBy(['category_id'=> $cat->id]) as $assoc) {
            array_push($posts, $postModel->findBy(['id' => $assoc->post_id])[0]);
        }
        
        return new Response($this->view(
            "layouts/category.html.twig",
            [
                'categoriesCollection' => $categoriesCollection,
                'image' => $image,
                'title' => $name,
                'nom' => $name,
                'description' => $desc,
                'postData' => $posts,
                'date' => $this->date

            ]
        ), 200);
        
    }

    /**
     * Upade a category
     *
     * @param Request $request
     * @return Response
     */
    public function updateCategory(Request $request): Response
    {
        // existing categories
        $categoriesCollection = $this->categoriesCollection;
        // check all admin
        $userModel = new UserModel();
        // check all admin
        $users = $userModel->findBy(['role' => 'Admin']);
        // array to stock them
        $adminArray = [];

        // put them in it
        foreach($users as $admin){
            array_push($adminArray, $admin->username);
        }
        // instanciate session
        $session = new Session();
        // check username in session
        if (!in_array($session->get('name'), $adminArray)) {
            return new Response($this->view(
                '404/404.html.twig', [
                    'message' => HelperMethods::notify('vous n\'avez pas accés a cette page', 'red', '#fff')
                ]
            ), 401);
        }
        // categories collection for menu
        $categoriesCollection = $this->categoriesCollection;

        // slug from request
        $slug = $request->attributes->get('slug');

        // category to edit
        $categoryToEdit = $this->model->findOneBy(['slug' => $slug]);

        // pass id in request
        $request->attributes->set('id', $categoryToEdit->id);

        // pass registerd image
        $request->attributes->set('actual_registered_image', $categoryToEdit->image_category);

        // data passed to form
        include __DIR__ . '\..\Utils\FormUpdateCategory.php';

        if($request->isMethod('POST')){
            $path = __DIR__ . '\..\..\public\uploads';

            // validator instanciation
            $validation = new Validator($request);

            // file to upload or existing file
            $image = $request->files->get('image_category');

            $errors = $validation->getErrors();

            if (empty($errors)) {
                $categoryModel = new CategoryModel();
                if (empty($image)) {
                    // set data with old image
                    $image = $categoryToEdit->image_category;
                    $settedCategory = $this->prepareCategory($validation, $image, $categoryModel);
                } else {
                    unlink(__DIR__ . "\..\..\public\uploads\\" . $categoryToEdit->image_category);
                    $image = $request->files->get('image_category');
                    $filesUploader = new FileUploader($path, $image, $validation);
                    $settedCategory = $this->prepareCategory($validation, $filesUploader->getFileName(), $categoryModel);
                }

                // set id in setted category
                $settedCategory->setId($request->attributes->get('id'));

                try {
                    $categoryModel->update($settedCategory->id, $categoryModel);
                    return new Response($this->view(
                        "layouts/categories.html.twig",[
                            'message' => HelperMethods::notify("mise à jour effectuée", "#00ff00", "#fff")
                        ]
                        ));
                } catch (PDOException $e) {
                    return new Response(
                        $this->view(
                            "404/404/html.twig",[
                                'message'=> $e->getMessage(),
                                'date' => $this->date
                            ]
                        )
                    );
                }
            }
        }
        

        return new Response($this->view(
            "layouts/create-update-category.html.twig", [
                "form" => $form->generate(),
                "categoriesCollection" => $categoriesCollection,
                'title' => $categoryToEdit->title,
                'slug' => $categoryToEdit->slug,
                'category_image' => $categoryToEdit->image_category,
                'content' => $categoryToEdit->content,
                'date' => $this->date
            ]
        ), 200);
    }

    public function deleteCategory(Request $request): Response
    {
        $session = new Session();
        $userModel = new UserModel();
        // check all admin
        $users = $userModel->findBy(['role' => 'Admin']);
        // array to stock them
        $adminArray = [];

        // put them in it
        foreach($users as $admin){
            array_push($adminArray, $admin->username);
        }

        // instanciate session
        if(!in_array($session->get('name'), $adminArray)){
            return new Response($this->view(
                "layouts/handle-connection.html.twig", [
                    'date' => $this->date
                ]
            ), 401);
        }

        // find slug
        $slug = $request->attributes->get('slug');
        // instanciate model
        $category = new CategoryModel();
        // find category to delete
        $categoryToDelete = $category->findOneBy(['slug' => $slug]);
        // title for notification
        $title = "Catégorie $categoryToDelete->title";

        if($request->isMethod('post')){
           try {
            $category->delete($categoryToDelete->id);
            return new Response($this->view(
                "layouts/success.html.twig", [
                    'message' => HelperMethods::notify("la catégorie $title à bien été supprimée",'red', '#fff' ),
                    'date' => $this->date
                ]
                ), 200);
           } catch (PDOException $e) {
            return new Response( $this->view(
                "404/404.html.twig",[
                    'message' => $e->getMessage(),
                    'date' => $this->date
                ]
            ), 500);
           }
        }

        include __DIR__ . '\..\Utils\FormDelete.php';

        return new Response($this->view(
            "layouts/delete.html.twig", [
                'title' =>$title,
                'form' => $form->generate(),
                'date' => $this->date
            ]
        ), 200);

    }

    
}