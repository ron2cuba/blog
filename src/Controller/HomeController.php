<?php

namespace App\Controller;

use Core\Controller;
use App\Database\PostModel;
use App\Database\CategoryModel;
use App\Database\UserModel;
use App\Services\ContactValidator;
use Helpers\HelperMethods;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class HomeController extends Controller{

    public function __construct()
    {
        $this->date = date('m-d-Y H:i:s');
    }
    public function renderHomePage()
    {
        $session = new Session();
        /**
         * init Post model to find latests
         */
        $posts = new PostModel;
        // find latests post
        $latests = $posts->findLatests(3);

        if ($session->get('name')) {
            $userModel = new UserModel();
            $connectedUser = $userModel->findOneBy(['username'=>$session->get('name')]);
            return new Response($this->view(
               'home/index.html.twig', [
                    'connectedUser'=> $connectedUser,
                    'latests'=> $latests,
                    'date' => $this->date
               ]
            ));

        }

        // userModel for changing info if nobody is connected
        $model = new UserModel();
        // get data of admin
        $user = $model->findOneBy(['username' => 'ron2cuba']);
        $image = $user->user_image;
        //categorieModel for menu
        $categories = new CategoryModel;

        $content = $user->content;
        $categoriesCollection = $categories->findAll();

        
        $request = new Request();
        include __DIR__ . '/../Utils/FormContact.php';

        return new Response($this->view(
            'home/index.html.twig',[
            'latests' => $latests,
            'categoriesCollection' => $categoriesCollection,
            'content' => $content,
            'form' => $form->generate(),
            'date' => $this->date
            ]
        ), 200);
    }

    public function contact(Request $request)
    {
        $categoryModel = new CategoryModel();
        $categoriesCollection = $categoryModel->findAll();
        // dd($categoriesCollection);
        if($request->isMethod('POST')){
            $validation = new ContactValidator($request);

            $errors = $validation->getErrors();
            if (empty($errors)) {
                $validation->sendEmail();
            }
        }

        return new Response($this->view(
            'layouts/success.html.twig',[
                'message' => HelperMethods::notify("Message envoyé", "#3dff3d", "#fff")
            ]
        ), 200);
    }

}