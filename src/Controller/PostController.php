<?php
declare(strict_types=1);
namespace App\Controller;
use Exception;
use PDOException;
use Core\DbManager;
use Core\Controller;
use Helpers\HelperMethods;
use App\Database\PostModel;
use App\Database\UserModel;
use App\Services\Validator;
use App\Services\FileUploader;
use App\Database\CategoryModel;
use App\Database\CommentModel;
use App\Database\PostCategoryModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class PostController extends Controller
{
    public function __construct(public $postModel = new PostModel(), public $categoryModel = new CategoryModel())
    {
        $this->categoriesCollection = $this->categoryModel->findAll();
        $this->date = date('d-m-Y');
    }
    
    public function createPost(Request $request)
    {
        $adminRights = 0;
        $session = new Session();
        $userModel = new UserModel();
        // check connexion
        if(!$session->get('name')){
            return new Response($this->view(
                '404/404.html.twig', [
                    'message'=>'vous devez etre authentifié pour poster',
                    'date' => $this->date
                ]
            ), 200);
        }
        // get data from name in session
        $connectedUser = $userModel->findOneBy(['slug' => HelperMethods::slugify($session->get('name'))]);
        // set user_id in request
        $session->set('user_id', $connectedUser->id);

        // pass categories list to form
        $categories = $this->passCategoriesToForm($this->categoriesCollection);
        
        // existing categories
        $categoriesCollection = $this->categoriesCollection;

        // title for template
        $title = "création d'un article";
        
        // check all admin
        $userModel = new UserModel();

        // check all admin
        $users = $userModel->findBy(['role' => 'Admin']);

        // array to stock them
        $adminArray = [];

        // put them in it
        foreach($users as $admin){
            array_push($adminArray, $admin->username);
        }
        if (in_array($session->get('name'), $adminArray)) {
            $adminRights = 1;
        }
        // form validation
        if($request->isMethod('POST')){

            //validator instanciation
            $validation = new Validator($request);
            // path for uploads
            $path = __DIR__ . '\..\..\public\uploads';
            // image field
            $image = $request->files->get('post_image');
            // error image
            if(empty($image)){
                // error in form
                $validation->hasError('image', 'une image est requise pour illustrer l\'article');
                // notify user
                HelperMethods::notify('une image doit illustrer votre catégorie', "#FF0000", "#fff");
            }
            // get errors
            $errors = $validation->getErrors();

            if (empty($errors)) {
                
                $postModel = new PostModel();
                // image processing
                $fileUploader = new FileUploader($path, $image, $validation);
                // pseudo hydration
                $settedPost = $this->preparePost($validation, $fileUploader->getFileName(), $postModel, $session->get('user_id'));

                $associatedCategory = new PostCategoryModel();

                $category = $request->request->get('categories');

                if ($image) {
                    try {
                        $postModel->create($settedPost);
                        $id = DbManager::getInstance()->lastInsertId();
                        $association = $associatedCategory->setPostId($id)
                        ->setCategoryId($category);
                        $associatedCategory->create($association);
                        
                    HelperMethods::notify("Enregistrement effectué", "#3dff3d", "#fff");
                    return new Response($this->view(
                        'layouts/success.html.twig', [
                            'message' => HelperMethods::notify("article créé avec succés", "#3dff3d", "#fff"),
                            'date' => $this->date
                        ]
                    )
                        ,200);
                
                    } catch (Exception $e) {
                    unlink(__DIR__ . "\..\..\public\uploads\{$fileUploader->getFileName()}");
                    return new Response($e->getMessage(), 500);
                    }
                }

            }
            
        }

        /**
         * Random data to pass in template
         */
        $title = "Créer un article";

        /**
         * create Form
         */
        include __DIR__ . '/../Utils/FormPost.php';

        $formTemplate = $form->generate();
        
        return new Response($this->view(
            "layouts/create-update-post.html.twig",[
                "categoriesCollection" => $this->categoriesCollection,
                "form" => $form->generate(),
                'title' => $title,
                'admin' => $adminRights,
                'date' => $this->date
            ]
        ), 200);
    }

    private function preparePost(Validator $validator, string $image,PostModel $model, int $session): PostModel
    {
        $session = new Session();

        $model->setTitle($validator->getSanitizedTitle())
        ->setSlug(HelperMethods::slugify($validator->getSanitizedTitle()))
        ->setPostImage($image)
        ->setContent($validator->getSanitizedTextarea())
        ->setIsPublished($validator->getIsPublished())
        ->setUpdatedAt($validator->getUpdatedAt())
        ->setUserId($session->get('user_id'));
        return $model;
    }

    public function getOnePostBySlug(Request $request)
    {
        $categories = new CategoryModel;

        $slug = $request->attributes->get('slug');
        
        // post data
        $postModel = new PostModel();

        if (!$postModel->findOneBy(['slug' => $slug])) {
            return new Response($this->view(
                '404/404.html.twig', [
                    'message' => 'l\'article n\'existe pas',
                    'date' => $this->date
                ]
            ), 404);
        }
        $accessToPost = $postModel->findOneBy(['slug' => $slug]);

        // commments data
        $commentsModel = new CommentModel();
        
        $categoryModel = new PostCategoryModel();
        $userModel = new UserModel();
        $owner = $userModel->findOneBy(['id' => $accessToPost->user_id]);
        
        $categoryId = $categoryModel->findBy(['post_id' => $accessToPost->id])[0]->category_id;

        $comments = $commentsModel->findBy(['post_id'=>$accessToPost->id]);
        $category = $categories->findBy(['id' => $categoryId]);

        $userModel = new UserModel();

        return new Response($this->view(
            "layouts/post.html.twig",[
                'category' => $category[0]->title,
                "categoriesCollection" => $this->categoriesCollection,
                'title' => $accessToPost->title,
                'content' => $accessToPost->content,
                'image' => $accessToPost->post_image,
                'createdAt' => $accessToPost->created_at,
                'is_published' => $accessToPost->is_published,
                'slug' => $slug,
                'comments' => $comments,
                'owner' =>  $owner->username,
                'ownerSlug' => $owner->slug
            ]
        ), 200);
        
    }
    
    public function updatePost(Request $request)
    {
        // instanciate Session
        $session = new Session();

        // check all admin
        $userModel = new UserModel();
        $users = $userModel->findBy(['role' => 'Admin']);

        // array to stock them
        $adminArray = [];

        // put them in it
        foreach($users as $admin){
            array_push($adminArray, $admin->username);
        }
        // get slug from request
        $slug = $request->attributes->get('slug');

        // if error on getting post
        if(!$this->postModel->findOneBy(['slug' => $slug])){
            return new Response($this->view(
                '404/404.html.twig', [
                    'message' => 'cet article n\'existe pas encore, écrivez le!',
                    'date' => $this->date
                ]
            ), 200);
        }
        // post to edit
        $postToEdit = $this->postModel->findOneBy(['slug' => $slug]);

        // pass post owner in attributes
        $request->attributes->set('post_owner', $postToEdit->user_id);

        // pass post_id in request attributes
        $request->attributes->set('post_id', $postToEdit->id);

        // model for associated category
        $postCategory = new PostCategoryModel();

        // category id 
        $originalCategoryId = $postCategory->findBy(['post_id' => $postToEdit->id])[0]->category_id;
        $request->attributes->set('originalCategoryId', $originalCategoryId);

        // find association for category
        $associations = $postCategory->findBy(['post_id'=>$postToEdit->id]);
        $allCatIds = [];
        // get all id's
        foreach ($associations as $association) {
            array_push($allCatIds, $association->category_id);
        }
        // get all name
        $categoryModel = new CategoryModel();

        foreach ($allCatIds as $id) {
            $categoryName = $categoryModel->findBy(['id', $id])[0]->title;
        }

        // all categories for value tag in form
        $categories = $this->passCategoriesToForm($this->categoriesCollection);

        // path for uploads
        $path = __DIR__ . '/../../public/uploads';

        // $form
        include __DIR__ . '\..\Utils\FormUpdatePost.php';

        if ($request->isMethod('POST')) {
            $validation = new Validator($request);
        }

        // if admin
        if(in_array($session->get('name'), $adminArray)){
            $admin = true;
            $postModel = new PostModel();
            // if validation exists
            if (!empty($validation)) {
                if($request->files->get('post_image')){
                    $fileUploader = new FileUploader($path, $request->files->get('post_image'), $validation);
                    $postModel->setPostImage($fileUploader->getFileName());
                } else {
                    $postModel->setPostImage($postToEdit->post_image);
                } 
                try {
                    $updatedPost = $postModel->setSlug(HelperMethods::slugify($validation->getSanitizedTitle()))
                    ->setTitle($validation->getSanitizedTitle())
                    ->setContent($validation->getSanitizedTextarea())
                    // ->setPostImage(isset($validation->post_image))
                    ->setId(isset($validation->id))
                    ->setUserId(isset($validation->user_id))
                    ->setCreatedAt(isset($validation->created_at))
                    ->setUserId($validation->getPostOwner())
                    ->setIsPublished(1);
                    $postModel->update($validation->getPostId(), $updatedPost);
                    return new Response($this->view(
                        'layouts/success.html.twig', [
                            'message'=> HelperMethods::notify("article créé avec succés", "#3dff3d", "#fff"),
                            'date' => $this->date
                        ]
                    ), 200);
                } catch (PdoException $e) {
                    return new Response($this->view(
                        '404/404.html.twig', [
                            'message' => $e->getMessage(),
                            'date' => $this->date
                        ]
                    ));
                }
            }
            return new Response($this->view(
                'layouts/create-update-post.html.twig', [
                    'message' => 'une erreur est survenue',
                    'admin' => true,
                    'form' => $form->generate()
                ]
            ), 500);
        // if post owner & validation exists
        } elseif ($session->get('user_id') === $postToEdit->id && !empty($validation)) {
            $admin = false;
            $postModel = new PostModel();
            $updatedPost = $postModel->setId($validation->getPostId())
            ->setUserId($session->get('user_id'))
            ->setTitle($validation->getSanitizedTitle())
            ->setContent($validation->getSanitizedTextarea())
            ->setSlug(HelperMethods::slugify($validation->getSanitizedTitle()))
            ->setCreatedAt($postToEdit->created_at)
            ->setIsPublished($validation->getIsPublished());
            // fileUploader
            if($request->files->get('post_image')){
                $fileUploader = new FileUploader($path, $request->files->get('post_image'), $validation);
                $postModel->setPostImage($fileUploader->getFileName());
            } else {
                $postModel->setPostImage($postToEdit->post_image);
            }
            try {
                $postModel->update($postToEdit->id, $updatedPost);
                return new Response($this->view(
                    'layouts/success.html.twig', [
                        'message'=> HelperMethods::notify("article créé avec succés", "#3dff3d", "#fff"),
                        'date' => $this->date
                    ]
                ),200);
            } catch (PdoException $e) {
                return new Response($this->view(
                    '404/404.html.twig', [
                        'message' => $e->getMessage(),
                        'date' => $this->date
                    ]
                    ));
            }
        } elseif (!$session->get('name') && $session->get('user_id') !== $postToEdit->user_id) {
                $admin = false;
                return new Response($this->view(
                    '404/404.html.twig', [
                        'message' => 'Vous n\'êtes pas titulaire de cet article',
                        'date' => $this->date
                    ]
                ) ,401);
        } else {
            
            $admin = false;

            include __DIR__ . '\..\Utils\FormUpdatePost.php';

            return new Response($this->view(
                'layouts/create-update-post.html.twig', [
                    'message' => 'une erreur est survenue',
                    'admin' => true,
                    'form' => $form->generate(),
                    'date' => $this->date
                ]
            ), 500);
        }
        

        return new Response($this->view(
            "404/404.html.twig", [
                'message' => 'vous n\'avez pas l\'autorisation', 
                'categoriesCollection' => $this->categoriesCollection,
                'date' => $this->date]
        ), 200);
    }

    public function deletePost(Request $request)
    {
        $slug = $request->attributes->get('slug');
        $postModel = new PostModel();
        $post = $postModel->findoneBy(['slug' => $slug]);
        $title = $post->title;

        include __DIR__ . '\..\Utils\FormDelete.php';

        if($request->isMethod('post')){
            try {
                $postModel->delete($post->id);
                return new Response(
                    $this->view(
                        "layouts/success.html.twig",[
                            'message' => HelperMethods::notify("l'article $title à bien été supprimé",'red', '#fff'),
                            'date' => $this->date
                        ]
                    ),200);
            } catch (PDOException $e) {
                return new Response($this->view(
                    '404/404.html/twig', [
                        'message'=>$e->getMessage(),
                        'date' => $this->date
                    ]
                ),500);
            }
        }

        return new Response(
            $this->view(
                "layouts/delete.html.twig",[
                    'title' => $title,
                    'form' => $form->generate(),
                    'date' => $this->date
                ]
            ),200);
    }
    
    /**
     * Pass categories collection to form for select
     *
     * @param array $categoriesCollection
     * @return array
     */
    private function passCategoriesToForm(array $categoriesCollection): array
    {
        $categories = [];
        
        foreach ($categoriesCollection as $cat) {
            array_push($categories, $cat);
        }
        return $categories;
    }

    public function publish(Request $request, PostModel $postModel = new PostModel())
    {
        $session = new Session();
        $userModel = new UserModel();

        $adminRights = 0;

        // check admin
        $admins = $userModel->findBy(['role'=>'Admin']);

        // array to stock them
        $adminArray = [];

        // put them in it
        foreach($admins as $admin){
            array_push($adminArray, $admin->username);
        }
        // check if user in array
        if (in_array($session->get('name'), $adminArray)) {
            // trouver le post
            $post = $postModel->findOneBy(['id' => $request->attributes->get('id')]);
            dump($postModel);
            $model = new PostModel();
            $postModel->setUserId($post->id)
            ->setTitle($post->title)
            ->setSlug(HelperMethods::slugify($post->title))
            ->setContent($post->content)
            ->setPostImage($post->post_image)
            ->setCreatedAt($post->created_at)
            ->setIsPublished(1);
            $model->update($post->id, $postModel);
            $adminRights = 1;

            
            return new Response($this->view(
                'layouts/success.html.twig',[
                    'user' => $session->get('name'),
                    'date' => $this->date
                ]
            ), 200);
        }

        return new Response($this->view(
            '404/404.html.twig',[
                'message' => 'vous n\'êtes pas autorisé a effectuer cette action'
            ]
        ), 200);

    }

    public function getAll()
    {
        $postModel = new PostModel;

        $posts = $postModel->findAll(['post']);
        return new Response($this->view(
            'layouts/posts.html.twig', [
                'poststCollection' => $posts,
                'title' => 'tous les posts',
                'date' => $this->date
            ]
        ), 200);
    }
}