<?php
declare(strict_types=1);

namespace App\Controller;

use PDOException;
use Core\Controller;
use App\Database\PostModel;
use App\Database\UserModel;
use App\Database\CommentModel;
use App\Services\CommentValidator;
use Helpers\HelperMethods;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class CommentController extends Controller
{
    public function createComment($request)
    {
        return include __DIR__ . '\..\Utils\FormComment.php';

    }

    public function create(Request $request)
    {
        $session =  new Session();
        $session->get('user_id');
        if(!$session->get('user_id')){
            return  new Response($this->view(
                '404/404.html.twig',[
                    'message'=>'vousdevez être authentifié pour commenter'
                ]
            ), 401);
        }
        // get slug in attributes
        $slug = $request->attributes->get('slug');
        // instanciate post model
        $postModel = new PostModel();
        // find id
        $post = $postModel->findOneBy(['slug' => $slug]);
        // form
        include __DIR__ . '\..\Utils\FormComment.php';
        // validation
        if ($request->isMethod('POST')) {
            $validation = new CommentValidator($request, $post->id, $session->get('user_id'));
            $validation->isComplete();
            $comment = new CommentModel();
            $comment->setTitle($validation->getTitle())
            ->setContent($validation->getContent())
            ->setCreatedAt($validation->getCreatedAt())
            ->setIsPublished($validation->getIsPublished())
            ->setUserId($validation->getUserId())
            ->setPostId($validation->getPostId());
            // persit
            try {
                $commentModel = new CommentModel();
                $commentModel->create($comment);
                return new Response($this->view(
                    'layouts/comments.html.twig',[
                        
                    ]
                ),200);
                
            } catch (PDOException $e) {
                return new Response($this->view(
                    '404/404.html.twig',[
                        'message'=> $e->getMessage()
                    ]
                ) ,500);
            }
        }
        

        return new Response($this->view(
            'layouts/comments.html.twig',[
                'slug' => $slug,
                'form' => $form->generate(),
                'title' => $post->title
            ]
        ),200);
    }

    public function validate(Request $request, CommentModel $commentModel = new CommentModel)
    {
        // instanciate session
        $session =  new Session();

        // check all admin
        $userModel = new UserModel();
        $users = $userModel->findBy(['role' => 'Admin']);

        // array to stock them
        $adminArray = [];

        // put them in it
        foreach($users as $admin){
            array_push($adminArray, $admin->username);
        }
        // check if admin
        if (in_array($session->get('name'), $adminArray)) {
            // instanciate comment Model
            $model = new CommentModel();
            
            // find comment by id
            $comment = $commentModel->find((int)$request->attributes->get('id'));

            // set comment
            try {
                $model->setUserId($comment->user_id)
                ->setTitle(htmlentities($comment->title))
                ->setContent($comment->content)
                ->setCreatedAt($comment->created_at)
                ->setModifiedAt($comment->modified_at)
                ->setIsPublished(1)
                ->setPostId($comment->post_id);
                $CommentModel = new CommentModel();
                $commentModel->update((int)$request->attributes->get('id'), $model);
                return new Response($this->view(
                    'layouts/success.html.twig', [
                        'message' => HelperMethods::notify('message validé', "#3dff3d", "#fff")
                    ]
                ),200);
            } catch (PDOException $e) {
                return new Response($this->view(
                    '404/404.html.twig', [
                        'message' => $e->getMessage()
                    ]
                ), 500);
            }

            return new Response($this->view(
                'layouts/success.html.twig', [
                    'message' => 'validé avec succès'
                ]
            ), 200);
        }

        return new Response($this->view(
            "404/404.html.twig",[
                'message' => 'vous n\'êtes pas autorisé pour cette action'
            ]
        ), 401);

    }
}
