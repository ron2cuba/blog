<?php
namespace App\Router;


use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 *  [Route]
 */
$routes = new RouteCollection;
// home
$routes->add('home', new Route("/", [
    '_controller' => 'App\Controller\HomeController::renderHomePage'
]));
// create post
$routes->add('createPost', new Route('/create/post', [
    '_controller' => 'App\Controller\PostController::createPost'
]));
// read a post
$routes->add('readPost', new Route('/post/{slug}', [
    '_controller' => 'App\Controller\PostController::getOnePostBySlug'
]));
// read all posts
$routes->add('readAll', new Route('/posts', [
    '_controller' => 'App\Controller\PostController::getAll'
]));
// update a post
$routes->add('updatePost', new Route('/update/post/{slug}', [
    '_controller' => 'App\Controller\PostController::updatePost'
]));
// delete a post
$routes->add('deletePost', new Route('/delete/post/{slug}', [
    '_controller' => 'App\Controller\PostController::deletePost'
]));
// list all categories
$routes->add('listCategories', new Route('/categories', [
    '_controller' => 'App\Controller\CategoryController::listAllCategories'
]));
// read a category
$routes->add('readCategory', new Route('/category/{slug}', [
    '_controller' => 'App\Controller\CategoryController::getOneCategoryBySlug'
]));
// create a category
$routes->add('createCategory', new Route('/create/category', [
    '_controller' => 'App\Controller\CategoryController::createCategory'
]));
// update a category
$routes->add('updateCategory', new Route('/update/category/{slug}', [
    '_controller' => 'App\Controller\CategoryController::updateCategory'
]));
// delete a category
$routes->add('deleteCategory', new Route('/delete/category/{slug}', [
    '_controller' => 'App\Controller\CategoryController::deleteCategory'
]));
// login
$routes->add('login', new Route('/log-in', [
    '_controller' => 'App\Controller\UserController::logIn'
]));
// register
$routes->add('register', new Route('/register', [
    '_controller' => 'App\Controller\UserController::register'
]));
// logout
$routes->add('logout', new Route('/logout', [
    '_controller' => 'App\Controller\UserController::logout'
]));
// consult profile
$routes->add('user', new Route('/user/{slug}', [
    '_controller' => 'App\Controller\UserController::getUserBySlug'
]));
// update profile
$routes->add('updateUser', new Route('/user/update/{slug}', [
    '_controller' => 'App\Controller\UserController::updateUser'
]));
// access admin
$routes->add('admin', new Route('/admin', [
    '_controller' => 'App\Controller\UserController::admin'
]));
// comment a post
$routes->add('createComment', new Route('/comments/create/{slug}', [
    '_controller' => 'App\Controller\CommentController::create'
]));
// validate a comment
$routes->add('validateComment', new Route('/comments/validate/{id}', [
    '_controller' => 'App\Controller\CommentController::validate'
]));
// publish a post
$routes->add('publish', new Route('/publish/{id}', [
    '_controller' => 'App\Controller\PostController::publish'
]));
// publish a post
$routes->add('contact', new Route('/contact', [
    '_controller' => 'App\Controller\HomeController::contact'
]));

return $routes;