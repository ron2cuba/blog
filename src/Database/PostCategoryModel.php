<?php

declare(strict_types=1);

namespace App\Database;

class PostCategoryModel extends Db
{
    protected $post_id;
    protected $category_id;

    public function __construct()
    {
        $this->table = 'post_category';
    }
    

    /**
     * Get the value of category_id
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * Set the value of category_id
     */
    public function setCategoryId($category_id): self
    {
        $this->category_id = $category_id;

        return $this;
    }

    /**
     * Get the value of post_id
     */
    public function getPostId()
    {
        return $this->post_id;
    }

    /**
     * Set the value of post_id
     */
    public function setPostId($post_id): self
    {
        $this->post_id = $post_id;

        return $this;
    }
}