<?php

declare(strict_types=1);

namespace App\Database;

class CommentModel extends Db
{
    protected $title;
    protected $content;
    protected $created_at;
    protected $modified_at;
    protected $user_id;
    protected $is_published;
    protected $post_id;
    
    public function __construct()
    {
        $this->table = 'comment';
    }

    /**
     * Get the value of title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     */
    public function setTitle($title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set the value of content
     */
    public function setContent($content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get the value of created_at
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set the value of created_at
     */
    public function setCreatedAt($created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get the value of modified_at
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * Set the value of modified_at
     */
    public function setModifiedAt($modified_at): self
    {
        $this->modified_at = $modified_at;

        return $this;
    }

    /**
     * Get the value of user_id
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set the value of user_id
     */
    public function setUserId($user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Get the value of is_published
     */
    public function getIsPublished()
    {
        return $this->is_published;
    }

    /**
     * Set the value of is_published
     */
    public function setIsPublished($is_published): self
    {
        $this->is_published = $is_published;

        return $this;
    }

    /**
     * Get the value of post_id
     */
    public function getPostId()
    {
        return $this->post_id;
    }

    /**
     * Set the value of post_id
     */
    public function setPostId($post_id): self
    {
        $this->post_id = $post_id;

        return $this;
    }
}