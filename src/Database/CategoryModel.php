<?php

declare(strict_types=1);

namespace App\Database;

class CategoryModel extends Db
{
    protected $title;
    protected $content;
    protected $slug;
    protected $image_category;
    protected $created_at;
    protected $modified_at;

    public function __construct()
    {
        $this->table = 'category';
    }

    public function __toString()
    {
        return $this->title;
    }

    /**
     * Set the value of id
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }
    

    /**
     * Get the value of title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     */
    public function setTitle($title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of slug
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    /**
     * Set the value of slug
     */
    public function setSlug($slug): self
    {
        $this->slug = $slug;
        
        return $this;
    }
    
    /**
     * Get the value of content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set the value of content
     */
    public function setContent($content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get the value of created_at
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set the value of created_at
     */
    public function setCreatedAt($created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get the value of modified_at
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * Set the value of modified_at
     */
    public function setModifiedAt($modified_at): self
    {
        $this->modified_at = $modified_at;

        return $this;
    }


    /**
     * Get the value of image_category
     */
    public function getImageCategory()
    {
        return $this->image_category;
    }

    /**
     * Set the value of image_category
     */
    public function setImageCategory($image_category): self
    {
        $this->image_category = $image_category;

        return $this;
    }
}