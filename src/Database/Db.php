<?php
declare(strict_types=1);
namespace App\Database;

use Core\DbManager;

class Db 
{
    protected $table;
    private $db;

    public function __construct()
    {}

    /**
     * Get all data in table
     *
     * @return array
     */
    public function findAll():array
    {
        return $this->execMyQuery('SELECT * FROM '.$this->table)->fetchAll();
    }

    /**
     * Find by criterias passed in array
     *
     * @param array $criterias
     * @return object
     */
    public function findBy(array $criterias): array
    {
        $fields = [];
        $values = [];

        // finbBy(['$field' => valeur])
        foreach ($criterias as $field => $value) {
            $fields[] = "$field = ?";
            $values[] = $value;
        }

        $fieldsList = implode(' AND ', $fields);
        $stmt =  $this->execMyQuery('SELECT * FROM '.$this->table.' WHERE ' . $fieldsList, $values);
        return $stmt->fetchAll();
    }

    /**
     * Find one element ex: ['id' => $is]
     *
     * @param array $criterias
     * @return object
     */
    public function findOneBy(array $criterias)
    {
        $fields = [];
        $values = [];

        // finbBy(['$field' => valeur])
        foreach ($criterias as $field => $value) {
            $fields[] = "$field = ?";
            $values[] = $value;
        }

        $fieldsList = implode(' AND ', $fields);
        $stmt =  $this->execMyQuery('SELECT * FROM '.$this->table.' WHERE ' . $fieldsList, $values);
        return $stmt->fetch();
    }

    /**
     * Find by id
     *
     * @param integer $id
     * @return object
     */
    public function find(int $id): object
    {
        return $this->execMyQuery("SELECT * FROM $this->table WHERE id = $id")
        ->fetch();
    }

    /**
     * Find latests
     *
     * @param integer $number Number of posts to display
     * @return array 
     */
    public function findLatests(int $number):array
    {
        return $this->execMyQuery("SELECT * FROM $this->table  WHERE 
        `is_published` = 1 ORDER BY `created_at` DESC LIMIT $number")->fetchAll();
    }
    
    /**
     * Create element
     *
     * @param Db $model class model to create
     * @return void
     */
    public function create(Db $model)
    {
        $fields = [];
        $values = [];
        $questionMarks = [];

        foreach($model as $field => $value){
            //remove null table and db in request
            if($value != null && $field != 'db' && $field != 'table'){
                $fields[] = $field;
                $questionMarks[] = "?";
                $values[] = $value;
            }
        }

        $fieldsList = implode(', ', $fields);
        $questionMarksList = implode(', ', $questionMarks);
        return $this->execMyQuery('INSERT INTO '.$this->table.' ( ' . $fieldsList. ') VALUES ('. $questionMarksList.')', $values);
    }
    
    public function update(int $id, Db $model)
    {
        $fields = [];
        $values = [];

        foreach($model as $field => $value){
            if($value != null && $field != 'db' && $field != 'table'){
                $fields[] = "$field = ?";
                $values[] = $value;
            }
        }
        $values[] = $id;

        $fieldsList = implode(', ', $fields);
        return $this->execMyQuery('UPDATE '.$this->table.' SET ' . $fieldsList. ' WHERE id = ?', $values);

    }

    /**
     * Update association table
     *
     * @param int $settedId
     * @param int $postId
     * @param int $categoryId
     * @return void
     */
    public function assocUpdate(Db $model, $settedId, $postId, $categoryId)
    {
        foreach ($model as $field => $value) {
            if($value != null && $field != 'db' && $field != 'table'){
                $fields[] = "$field = ?";
                $values[] = $value;
            }
        }
        $values = [$settedId, $postId, $categoryId];

        return $this->execMyQuery("UPDATE `post_category` SET `category_id` = ? WHERE `post_category`.`post_id` = ? AND `post_category`.`category_id` = ?", $values);
    }

    public function delete(int $id)
    {
        return $this->execMyQuery("DELETE FROM $this->table WHERE `id` = ?", [$id]);
    }

    private function execMyQuery(string $sql, array $attributes = null)
    {
        $this->db = DbManager::getInstance();
        
        if($attributes !== null){
            $query = $this->db->prepare($sql);
            $query->execute($attributes);
            return $query;
        } else {
            return $this->db->query($sql);
        }
    }
}
