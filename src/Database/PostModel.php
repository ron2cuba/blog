<?php
declare(strict_types=1);

namespace App\Database;

class PostModel extends Db
{
    protected $id;
    protected $user_id;
    protected $title;
    protected $content;
    protected $slug;
    protected $post_image;
    protected $created_at;
    protected $updated_at;
    protected $is_published;
    
    public function __construct()
    {
        $this->table = 'post';
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     */
    public function setTitle($title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set the value of content
     */
    public function setContent($content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get the value of slug
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set the value of slug
     */
    public function setSlug($slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get the value of created_at
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set the value of created_at
     */
    public function setCreatedAt($created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get the value of is_published
     */
    public function getIsPublished()
    {
        return $this->is_published;
    }

    /**
     * Set the value of is_published
     */
    public function setIsPublished($is_published): self
    {
        $this->is_published = $is_published;

        return $this;
    }

    /**
     * Get the value of post_image
     */
    public function getPostImage()
    {
        return $this->post_image;
    }

    /**
     * Set the value of post_image
     */
    public function setPostImage($post_image): self
    {
        $this->post_image = $post_image;

        return $this;
    }

    /**
     * Get the value of user_id
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set the value of user_id
     */
    public function setUserId($user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Get the value of updated_at
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set the value of updated_at
     */
    public function setUpdatedAt($updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }
}