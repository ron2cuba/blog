-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : dim. 31 juil. 2022 à 12:20
-- Version du serveur : 8.0.27
-- Version de PHP : 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `blog_p5`
--

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `image_category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `title`, `slug`, `image_category`, `content`) VALUES
(1, 'Les Simpsons', 'les-simpsons', 'simpsons-62e4335b75852.jpg', 'Les Simpson (The Simpsons) sont une s&eacute;rie t&eacute;l&eacute;vis&eacute;e d&#039;animation am&eacute;ricaine1 cr&eacute;&eacute;e par Matt Groening et diffus&eacute;e pour la premi&egrave;re fois &agrave; partir du 17 d&eacute;cembre 1989 sur le r&eacute;seau Fox. En 2020, elle est diffus&eacute;e sur Disney+.\r\n\r\nElle met en sc&egrave;ne les Simpson, st&eacute;r&eacute;otype d&#039;une famille de classe moyenne am&eacute;ricaine2. Leurs aventures servent une satire du mode de vie am&eacute;ricain. Les membres de la famille, sont Homer, Marge, Bart, Lisa, Maggie, ainsi que Abe, le p&egrave;re d&#039;Homer.\r\n\r\nDepuis ses d&eacute;buts, la s&eacute;rie a r&eacute;colt&eacute; des dizaines de r&eacute;compenses, dont trente-deux Primetime Emmy Awards, trente Annie Awards et un Peabody Award. Le Time Magazine du 31 d&eacute;cembre 1999 l&#039;a d&eacute;sign&eacute;e comme la meilleure s&eacute;rie t&eacute;l&eacute;vis&eacute;e du xxe si&egrave;cle et elle a obtenu une &eacute;toile sur le Hollywood Walk of Fame le 14 janvier 2000. &laquo; D&#039;oh! &raquo;, l&#039;expression d&#039;abattement d&#039;Homer Simpson, est entr&eacute;e dans la langue anglaise. Ce n&#039;est pas le seul mot &agrave; &ecirc;tre entr&eacute; dans le dictionnaire anglais, embiggen (&laquo; engrandi &raquo; en fran&ccedil;ais) est un mot invent&eacute; par les Simpson qui est aussi entr&eacute; dans la langue anglaise. L&#039;influence des Simpson s&#039;exerce &eacute;galement sur d&#039;autres sitcoms.'),
(2, 'categorie de test', 'categorie-de-test', 'a-62e44daec8d30.png', 'zerhqerj');

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT NULL,
  `is_published` tinyint NOT NULL DEFAULT '0',
  `post_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `comment_ibfk_1` (`user_id`),
  KEY `fk_comment_post1_idx` (`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `post`
--

DROP TABLE IF EXISTS `post`;
CREATE TABLE IF NOT EXISTS `post` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `post_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_published` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `post`
--

INSERT INTO `post` (`id`, `user_id`, `title`, `slug`, `content`, `post_image`, `created_at`, `updated_at`, `is_published`) VALUES
(1, 1, 'Homer Simpson', 'homer-simpson', 'Homer est le p&egrave;re de la famille Simpson. Avec sa femme, Marge, ils ont trois enfants  : Bart (10 ans), Lisa (8 ans) et Maggie (2 ans). Dans l&#039;&eacute;pisode Na&icirc;tre ou ne pas na&icirc;tre (saison 24, &eacute;pisode 3) on apprend que, jeune, pour avoir de l&#039;argent, il a fait plusieurs dons &agrave; la banque de sperme de Shelbyville. Il en aurait fait assez pour se payer une Corvette, on voit d&#039;ailleurs un tableau rempli de photos de &laquo; mini-Homer &raquo; : blancs, noirs, filles, gar&ccedil;ons&hellip; et m&ecirc;me septupl&eacute;s. Il travaille &agrave; la centrale nucl&eacute;aire de Springfield et incarne le st&eacute;r&eacute;otype am&eacute;ricain de la classe ouvri&egrave;re ; il est vulgaire, alcoolique, en surpoids, incomp&eacute;tent, maladroit, paresseux, ignorant mais est cependant d&eacute;vou&eacute; &agrave; sa famille. En d&eacute;pit de la routine de sa vie d&#039;ouvrier de banlieue, il a eu un grand nombre d&#039;aventures extraordinaires.', 'a-62e52d36cbb42.png', '2022-07-29 19:36:38', NULL, 1),
(2, 2, 'Marge Simpson', 'marge-simpson', 'Marge est la m&egrave;re bien intentionn&eacute;e et tr&egrave;s patiente de la famille Simpson. Avec son mari, Homer, elle a trois enfants : Bart, Lisa et Maggie. Marge est la force moralisatrice de sa famille et essaie souvent de maintenir l&#039;ordre dans la maison des Simpson. Elle est pr&eacute;sent&eacute;e comme le st&eacute;r&eacute;otype t&eacute;l&eacute;vis&eacute; de la m&egrave;re au foyer et est souvent pr&eacute;sente dans les listes des meilleures m&egrave;res de t&eacute;l&eacute;vision. Elle est apparue dans d&#039;autres m&eacute;dias sur le th&egrave;me des Simpson, dont des jeux vid&eacute;o, un film, The Simpsons Ride, des publicit&eacute;s et des bandes dessin&eacute;es, et a inspir&eacute; toute une gamme de produits d&eacute;riv&eacute;s.', 'marge-62e4f7cbde772.jpg', '2022-07-29 19:58:19', NULL, 0),
(3, 1, 'ceci est un post de test', 'ceci-est-un-post-de-test', 'description de test', 'b-62e503da78040.jpg', '2022-07-30 10:11:38', NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `post_category`
--

DROP TABLE IF EXISTS `post_category`;
CREATE TABLE IF NOT EXISTS `post_category` (
  `post_id` int NOT NULL,
  `category_id` int NOT NULL,
  PRIMARY KEY (`post_id`,`category_id`),
  KEY `fk_post_category_post_idx` (`post_id`),
  KEY `fk_post_category_category1_idx` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `post_category`
--

INSERT INTO `post_category` (`post_id`, `category_id`) VALUES
(1, 1),
(2, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` char(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `role` enum('Author','Admin','Subscriber') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'Subscriber',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `slug`, `content`, `user_image`, `email`, `role`, `created_at`) VALUES
(1, 'ron2cuba', '$2y$09$Lv0GpJMFVaz7dOFHttpX6.bf5rl80.Ah2KN4yzLLxRdHAtnZyddte', 'ron2cuba', 'Ronan Lenouvel, en reconversion professionnelle. J\'ai toujours été un \'bidouillleur\' avec les technos web, j\'ai décidé de faire d\'une passion mon métier!', 'lugh-62e3eea8bb23d.jpg', 'ronan.lenouvel@gmail.com', 'Admin', '2022-07-29 12:28:56'),
(2, 'Bill Murray', '$2y$09$SE3I.INUAB7awjauf06CJuNgF9gopiRq61bAfkCEVh8ah1TvaFJje', 'bill-murray', 'William Murray, dit Bill Murray, n&eacute; le 21 septembre 1950 &agrave; Evanston (Illinois), est un acteur, humoriste et r&eacute;alisateur am&eacute;ricain.\r\n\r\nIl est r&eacute;v&eacute;l&eacute; au public am&eacute;ricain par ses prestations comiques dans l&#039;&eacute;mission t&eacute;l&eacute;vis&eacute;e Saturday Night Live et joue ensuite dans plusieurs com&eacute;dies &agrave; succ&egrave;s, dont les films SOS Fant&ocirc;mes (1984, 1989), dans lesquels il interpr&egrave;te le Dr Peter Venkman et Un jour sans fin (1993). Il aborde par la suite d&#039;autres registres, et son r&ocirc;le dans Lost in Translation (2003) de Sofia Coppola lui vaut plusieurs r&eacute;compenses et une nomination &agrave; l&#039;Oscar du meilleur acteur. Par la suite, il continue dans le registre dramatique, en apparaissant notamment dans Broken Flowers (2005), St. Vincent et Olive Kitteridge (2014). Il appara&icirc;t dans tous les films de Wes Anderson depuis Rushmore (1998) et a collabor&eacute; avec Jim Jarmusch dans quatre de ses films.\r\n\r\nIl pr&ecirc;te, de mani&egrave;re occasionnelle, sa voix &agrave; des personnages, dont notamment Garfield dans Garfield le film (2004), &agrave; Clive Badger dans Fantastic Mr. Fox (2009), Baloo dans le remake de 2016 du film d&#039;animation Le Livre de la jungle (1967), ou encore &agrave; Boss dans L&#039;&Icirc;le aux chiens (2018).\r\n\r\nIl re&ccedil;oit en 2017 le prix Mark-Twain de l&#039;humour am&eacute;ricain.', 'bill-murray-62e40fadc3b4c.jpg', 'bill.murray@star.com', 'Subscriber', '2022-07-29 14:49:49'),
(3, 'admin de secours', '$2y$09$SDN60vCs8lu1jkzvk65h3.ovEQFGKY82BsSvWm6a7GjKkTpxduOle', 'admin-de-secours', 'je suis l&#039;admin de secours', 'b-62e4127e83239.jpg', 'admin@admin.com', 'Admin', '2022-07-29 15:01:50');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
