-- rapid cleaning
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE post_category;
TRUNCATE post_tag;
TRUNCATE post;
TRUNCATE user;
TRUNCATE user;
TRUNCATE category;
TRUNCATE comment;
SET FOREIGN_KEY_CHECKS = 1;