<?php
declare(strict_types=1);

namespace Core;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class EngineController extends Controller{

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request): Response
    {
        /**
         * @var Object baseUrl, pathInfo, method
         */
        $context = new RequestContext();
        $context->fromRequest($request);

        /**
         *  @var [Route]
         */
        $routes = require(__DIR__ .'/../src/routes.php');

        /**
         * UrlMatcher matches URL based on a set of routes
         * @var Object
         */
        $urlMatcher = new UrlMatcher($routes, $context);

        $controllerResolver = new ControllerResolver();
        $argumentResolver = new ArgumentResolver();
        
        /**
         * @var string pathInfo
         */
        $pathInfo = $request->getPathInfo();

        try {    
            /**
             * add result from url macther, [id, _route], in request parambag 
             */
            $request->attributes->add($urlMatcher->match($pathInfo));

            /**
             * @var array callabe
             */
            $controller = $controllerResolver->getController($request);

            /**
             * @var array Find arguments from request by reflexion
             */
            $arguments = $argumentResolver->getArguments($request, $controller);

            /**
             * @var Object "callable" and its "params"
             */
            $response = call_user_func_array($controller, $arguments);

        } catch(ResourceNotFoundException $e) {
            return new Response($this->view(
                '404/404.html.twig',[
                    'message' => $e->getMessage()
                ]
            ),404);
        }
        return $response;
    }

}