<?php
declare(strict_types=1);
namespace Core;

use PDO;
use PDOException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Handle PDO
 */
class DbManager extends PDO{
    
    private static $instance = null;

    private const HOST = "localhost";
    private const USERNAME = "root";
    private const PASSWORD = "";
    private const DBNAME = "blog_p5";

    private function __construct()
    {

        try {
            parent::__construct('mysql:host='.self::HOST.';dbname='.self::DBNAME,
            self::USERNAME,
            self::PASSWORD,
            );
            $this->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES utf8');
            $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            return new Response('erreur: '.$e->getMessage());
        }
    }
    
    /**
     * Generate DbManager if not exists or get actual instance if exists
     *
     * @return DbManager
     */
    public static function getInstance():DbManager
    {
        if(self::$instance === null){
            self::$instance = new DbManager();
        }
        return self::$instance;
    }
}
