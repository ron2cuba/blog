<?php
declare(strict_types=1);
namespace Core;

use Twig\Environment;
use AppBundle\AppExtension;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;

abstract class Controller
{
    protected $twig;

    /**
     * rendering temlpate files
     *
     * @param string $path
     * @param array $data
     * @return string
     */
    public function view($path, $data = []):string
    {
        $loader = new FilesystemLoader(__DIR__ .'/../templates');
        $twig = new Environment($loader, [
            'cache'=> false,
            'debug' => true,
        ]);
        //add function htmlentitydecode
        $function = new \Twig\TwigFunction('html_entity_decode', function ($text) {
            return html_entity_decode($text);
        });
        // add funcxtion excerpt
        $excerpt = new \Twig\TwigFunction('excerpt', function ($text) {
            return substr($text, 0, 50);
        });

        $twig->addFunction($function);
        $twig->addFunction($excerpt);
        
        $twig->addExtension(new \Twig\Extension\DebugExtension());

        return $twig->render($path, $data);
    }
    
    public function htmlEntityDecode(string $char)
    {
        return html_entity_decode($char);
    }
}