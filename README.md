# Blog P5 formation openclassrooms developer PHP/Symfony

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/77873db6ac574e16af19e0166ddfc9b6)](https://www.codacy.com/gl/ron2cuba/blog/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=ron2cuba/blog&amp;utm_campaign=Badge_Grade)

## Development environment

- [vscodium](https://github.com/VSCodium/vscodium/blob/master/DOCS.md) [download](https://sourceforge.net/projects/vscodium.mirror/)
- [Wamp](https://www.wampserver.com)

### Requirements:
<ul>
    <li>Apache <span style="color: red; font-size: .9rem">⚠️needs Apache for the redirection</span></li>
    <li>Mysql</li>
    <li>PhpMyadmin 5.2.0</li>
    <li>Php 8.1.6</li>
    <li>Wamp</li>
    <li>MysqlWorkbench</li>
    <li>PlantUml</li>
    <li>Codium</li>
    <li>GitLab</li>
</ul>

# Installation
- go to `phpMyAdmin`
- In database tab, create new database with the name `blog_p5` (encoding utf8mb4_0900_ai_ci).
- Select blog_p5 in list (left column)
- In SQL tab copy all the [dump sql content](#dump)
- Check if [Wamp](https://www.wampserver.com) (or other server with apache) is correctly launched
- In the address bar of your [your web browser :](https://www.google.com/chrome/)

![browser address bar](./public/images/browser.jpg "in your browser")

# Manual

1. launch wamp (or other local server with <span style="color: red; font-size: 1.1rem"><u>Apache</u> ⚠️</span>)
2. in your web Browser go to your localhost, in wamp click on the icon in systray and select virtualhost,
handle virtualhost.
3. in the first field enter blog, in the second field paste the path to your blog folder.
4. select PHP 8.1.6
5. click on create or modify
6. when all these operations are done in your web browser type blog

# User manual
1. home page is public
2. read a category's content is public
3. posts page is public too
4. for creating a post, you need to be registered
5. for updating a post, you must be the owner or administrator
6. aministrator has to be connected
7. administrator have to validate a post
8. administrator handle posts, categories, messages

admin account:
```txt
pseudo: ron2cuba
pasword: 0000 
```

user account
```txt
pseudo: Bill Murray
password: 0000 
```
admin panel
```txt
/blog/admin
```
### Sitemap

```txt
/blog -> accueil

/blog/categories -> list all categories

/blog/categorie/{category-title} -> category details

/blog/create/category -> create category

/blog/update/category/{category-name} -> update category

/blog/delete/category/{category-id} -> delete category

/blog/posts/ -> lists all posts

/blog/post/{post-name} -> post details

/blog/update/post/{post-name} -> update post

/blog/create/post -> create post

/blog/delete/post/{post-id} -> delete post

/blog/delete/category/{category-title} -> delete post

/blog/register -> register

/blog/log-in -> connect

/blog/loout -> deconnexion

/blog/user/{username} -> user details

/blog/user/update/{username} -> update user infos

/blog/admin -> admin panel
```

# Dump

[Dump SQl](./sql/blog_p5.sql)